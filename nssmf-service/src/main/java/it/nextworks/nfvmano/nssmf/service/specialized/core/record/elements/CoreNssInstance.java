package it.nextworks.nfvmano.nssmf.service.specialized.core.record.elements;

import com.fasterxml.jackson.annotation.JsonInclude;
import it.nextworks.nfvmano.nssmf.service.specialized.core.smf.models.NetworkSliceType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class CoreNssInstance {
    @Id
    @GeneratedValue
    private Long id;


    private UUID nssiId;

    private String n6CoreNetworkId;

    private String n6CoreSubnetId;

    private String n9NetworkId;

    private String n9SubnetId;

    private String n6EdgeNetworkId;

    private String n6EdgeSubnetId;
    private String n3EdgeNetworkId;

    private String n3EdgeSubnetId;

    private UUID networkServiceInstanceId;

    private NetworkSliceType networkSliceType;

    public String getN9SubnetId() {
        return n9SubnetId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "coreNssInstance", cascade= CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<CoreVnfAddressAlloc> coreVnfAddressAllocs = new ArrayList<>();
    public CoreNssInstance() {
    }

    public CoreNssInstance(UUID nssiId,
                           String n6CoreNetworkId,
                           String n6CoreSubnetId,
                           String n9NetworkId,
                           String n9SubnetId,
                           String n6EdgeNetworkId,
                           String n6EdgeSubnetId,
                           String n3EdgeNetworkId,
                           String n3EdgeSubnetId,
                           UUID networkServiceInstanceId, NetworkSliceType networkSliceType) {
        this.nssiId = nssiId;
        this.n6CoreNetworkId = n6CoreNetworkId;
        this.n6CoreSubnetId = n6CoreSubnetId;
        this.n9NetworkId = n9NetworkId;
        this.n6EdgeNetworkId = n6EdgeNetworkId;
        this.n6EdgeSubnetId = n6EdgeSubnetId;
        this.n3EdgeNetworkId = n3EdgeNetworkId;
        this.n3EdgeSubnetId = n3EdgeSubnetId;
        this.networkServiceInstanceId = networkServiceInstanceId;
        this.n9SubnetId=n9SubnetId;
        this.networkSliceType = networkSliceType;
    }

    public String getN3EdgeNetworkId() {
        return n3EdgeNetworkId;
    }

    public String getN3EdgeSubnetId() {
        return n3EdgeSubnetId;
    }

    public NetworkSliceType getNetworkSliceType() {
        return networkSliceType;
    }

    public UUID getNssiId() {
        return nssiId;
    }

    public String getN6CoreNetworkId() {
        return n6CoreNetworkId;
    }

    public String getN6CoreSubnetId() {
        return n6CoreSubnetId;
    }

    public String getN9NetworkId() {
        return n9NetworkId;
    }

    public String getN6EdgeNetworkId() {
        return n6EdgeNetworkId;
    }

    public String getN6EdgeSubnetId() {
        return n6EdgeSubnetId;
    }

    public UUID getNetworkServiceInstanceId() {
        return networkServiceInstanceId;
    }

    public List<CoreVnfAddressAlloc> getCoreVnfAddressAllocs() {
        return coreVnfAddressAllocs;
    }

    public void addCoreVnfAddressAlloc(CoreVnfAddressAlloc vnfAddressAlloc){
        this.coreVnfAddressAllocs.add(vnfAddressAlloc);
    }
}

package it.nextworks.nfvmano.nssmf.service.specialized.core.record.repo;

import it.nextworks.nfvmano.nssmf.service.specialized.core.record.elements.CoreNssInstance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface CoreNssInstanceRepository extends JpaRepository<CoreNssInstance, Long> {

    Optional<CoreNssInstance> findByNssiId(UUID nssiId);
}

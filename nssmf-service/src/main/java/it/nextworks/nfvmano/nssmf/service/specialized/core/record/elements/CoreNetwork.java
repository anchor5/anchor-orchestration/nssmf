package it.nextworks.nfvmano.nssmf.service.specialized.core.record.elements;

public enum CoreNetwork {
    N3,
    N6,
    N4,
    N9,
    OTHER
}

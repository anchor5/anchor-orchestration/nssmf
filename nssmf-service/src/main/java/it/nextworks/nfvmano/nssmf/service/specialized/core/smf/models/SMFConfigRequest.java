package it.nextworks.nfvmano.nssmf.service.specialized.core.smf.models;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;

public class SMFConfigRequest {
    @JsonProperty("network-slice-type")
    public NetworkSliceTypeObject networkSliceType;
    @JsonProperty("network-slice-parameters")
    public NetworkSliceParameters networkSliceParameters;
    @JsonProperty("network-slice-id")
    public String networkSliceId;
    public ArrayList<Upf> upfs;

    public SMFConfigRequest() {
    }

    public NetworkSliceTypeObject getNetworkSliceType() {
        return networkSliceType;
    }

    public void setNetworkSliceType(NetworkSliceTypeObject networkSliceType) {
        this.networkSliceType = networkSliceType;
    }

    public NetworkSliceParameters getNetworkSliceParameters() {
        return networkSliceParameters;
    }

    public void setNetworkSliceParameters(NetworkSliceParameters networkSliceParameters) {
        this.networkSliceParameters = networkSliceParameters;
    }

    public String getNetworkSliceId() {
        return networkSliceId;
    }

    public void setNetworkSliceId(String networkSliceId) {
        this.networkSliceId = networkSliceId;
    }

    public ArrayList<Upf> getUpfs() {
        return upfs;
    }

    public void setUpfs(ArrayList<Upf> upfs) {
        this.upfs = upfs;
    }
}

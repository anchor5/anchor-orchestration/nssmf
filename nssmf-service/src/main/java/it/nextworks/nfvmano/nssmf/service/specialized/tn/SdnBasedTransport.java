/*
 * Copyright (c) 2022 Nextworks s.r.l.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.nextworks.nfvmano.nssmf.service.specialized.tn;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.configuration.ConfigurationActionType;
import it.nextworks.nfvmano.libs.vs.common.nssmf.messages.specialized.transport.SdnConfigPayload;
import it.nextworks.nfvmano.nssmf.service.messages.notification.NssiStatusChangeNotiticationMessage;
import it.nextworks.nfvmano.nssmf.service.messages.provisioning.InstantiateNssiRequestMessage;
import it.nextworks.nfvmano.nssmf.service.messages.provisioning.ModifyNssiRequestMessage;
import it.nextworks.nfvmano.nssmf.service.nssmanagement.NssLcmEventHandler;
import org.json.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;



public class SdnBasedTransport extends NssLcmEventHandler {

    /*
        Topology assumptions
        - Port 1 -> OS i/o
        - Port 2 -> SAT
        - Port 3 -> TER
     */

    private MockOSDriver osDriver = new MockOSDriver();
    private MockSdnDriver sdnDriver = new MockSdnDriver();
    private MockStarfishDriver starfishDriver = new MockStarfishDriver();
    private OpenFlowConfigAssembler flowConfigAssembler = new OpenFlowConfigAssembler();
    public SdnBasedTransport(){
        this.setEnableAutoNotification(true);
    }

    @Override
    protected void processInstantiateNssRequest(InstantiateNssiRequestMessage message) throws NotExistingEntityException {
        SdnConfigPayload payload = (SdnConfigPayload)message.getInstantiateNssiRequest();
        //Step1 Get Vlan ID by Subslice ID from OpenStack
        log.info("Get Transport configuration requests - Transport slice ID: " + this.getNetworkSubSliceInstanceId());
        log.info("Target Slice ID: "+ payload.getTargetNssiId().toString());
        log.info("Target Transport ID: "+ payload.getTargetTransportId());
        log.info("Retrieve VLAN ID for network N9_net_"+ payload.getTargetNssiId());
        try {
            int vlanId = osDriver.retrieveVlanNetByName("N9_net_" + payload.getTargetNssiId());
            log.info("VLAN ID: " + vlanId);

            //Step2 Apply config

            log.info("Applying configuration on Switch SW1");
            log.info("Traffic match rule:");
            log.info("\n"+flowConfigAssembler.vlanMatch("1", vlanId, true, 1).toString(2));
            log.info("Actions on match:");
            log.info("\n"+flowConfigAssembler.outPort("3").toString(2));
            log.info("\nApplying configuration on Switch SW2");
            log.info("Traffic match rule:\n");
            log.info("\n"+flowConfigAssembler.vlanMatch("1", vlanId, true, 1).toString(2));
            log.info("Actions on match:\n");
            log.info("\n"+flowConfigAssembler.outPort("3").toString(2));
            log.info("\n");
            Map<String, String> satMap = new HashMap<>();
            for(Map<String, String> specs: payload.getTransportSpecifications())
                if(specs.get("transport-id").equals("SATELLITE"))
                    satMap = specs;
            log.info("Request Slice to SATCOM. GW ID: "+ satMap.get("gateway-id"));
            int vlan = starfishDriver.createSlice(this.getNetworkSubSliceInstanceId(), satMap.get("gateway-id"));
            log.info("Slice created on SATCOM. VLAN ID: " + vlan);
            log.info("\n");
            //sdnDriver.applyConfig(this.getNetworkSubSliceInstanceId().toString(), config);
            NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
            notif.setSuccess(true);
            this.getEventBus().post(notif);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void processModifyNssRequest(ModifyNssiRequestMessage message) throws NotExistingEntityException {
        SdnConfigPayload payload = (SdnConfigPayload)message.getModifyNssiRequest();
        //Step1 Get Vlan ID by Subslice ID from OpenStack

        log.info("Get Transport configuration requests - Transport slice ID: " + this.getNetworkSubSliceInstanceId());
        //log.info("Target Slice ID: "+ payload.getTargetNssiId());
        log.info("Requested Action: " + payload.getConfigurationActionType());

        if(payload.getConfigurationActionType() == ConfigurationActionType.SLICE_TRANSFER) {
            if(payload.getTransportConfig() != null)
                log.info(""+payload.getTransportConfig());

            log.info("Retrieve existing config from SDN Controller for slice " + this.getNetworkSubSliceInstanceId());

            //Step2 Apply config
            try {
                String config = sdnDriver.getConfig(this.getNetworkSubSliceInstanceId().toString());
                log.info("Switch from TERRESTRIAL to SATELLITE backhaul");
                int satVlanId = starfishDriver.getVlan(this.getNetworkSubSliceInstanceId());
                int n9VlanId = osDriver.retrieveVlanNetByName("vlan");
                log.info("SATCOM VLAN ID: " + satVlanId);
                log.info("N9 VLAN: " + n9VlanId);
                log.info("Applying configuration on Switch SW1");
                log.info("Traffic match rule I-UPF to BackHaul");
                log.info("\n"+flowConfigAssembler.vlanMatch("1", n9VlanId, true, 1).toString(2));
                log.info("Actions on match:");
                log.info("\n"+flowConfigAssembler.vlanPush("2", satVlanId, true).toString(2));
                log.info("Traffic match rule BackHaul to I-UPF");
                log.info("\n"+flowConfigAssembler.vlanMatch("2", satVlanId, true, 1).toString(2));
                log.info("Actions on match:");
                log.info("\n"+flowConfigAssembler.vlanPush("1", n9VlanId, true).toString(2));
                log.info("\n");
                log.info("Applying configuration on Switch SW2");
                log.info("Traffic match rule A-UPF to BackHaul");
                log.info("\n"+flowConfigAssembler.vlanMatch("1", n9VlanId, true, 1).toString(2));
                log.info("Actions on match:");
                log.info("\n"+flowConfigAssembler.vlanPush("2", satVlanId, true).toString(2));
                log.info("Traffic match rule BackHaul to A-UPF");
                log.info("\n"+flowConfigAssembler.vlanMatch("2", satVlanId, true, 1).toString(2));
                log.info("Actions on match:");
                log.info("\n"+flowConfigAssembler.vlanPush("1", n9VlanId, true).toString(2));


                if(payload.getTransportConfig() != null) {
                    log.info("Applying SATELLITE configuration");
                    log.info("" + payload.getTransportConfig());
                }
                log.info("Configuration Applied");
                log.info("\n");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }else{
            log.info("Applying SATELLITE configuration");
            if(payload.getTransportConfig() != null)

                log.info("SAT_GW: "+payload.getTransportConfig().getSatelliteGw());
            log.info("Configuration Applied");
            log.info("\n");
        }
        NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
        notif.setSuccess(true);
        this.getEventBus().post(notif);
    }
}

class MockStarfishDriver{
    int interval1 = 1000;
    int interval2 = 2000;
    int interval3 = 3000;

    private Map<UUID, Integer> sliceMap = new HashMap<>();

    int createSlice(UUID sliceId, String gwId) throws InterruptedException {
        Thread.sleep(interval2);
        sliceMap.put(sliceId, 1101);
        return 1101;
    }
    int getVlan(UUID sliceId){
        return sliceMap.get(sliceId);
    }
}
class MockOSDriver{
    int interval1 = 1000;
    int interval2 = 2000;
    int interval3 = 3000;


    int retrieveVlanNetByName(String netName) throws InterruptedException {
        Thread.sleep(interval2);
        return 1001;
    }
}

class OpenFlowConfigAssembler{
    private JSONObject config;

    OpenFlowConfigAssembler(){

    }

    JSONObject vlanMatch(String inPort, int vlanId, boolean vlanIdPresent, int vlanPcp){

        String conf = "{\n" +
                "\"in-port\": \""+inPort+"\",\n" +
                "\"vlan-match\": {\n" +
                "                     \"vlan-id\": {\n" +
                "                         \"vlan-id-present\": "+vlanIdPresent+",\n" +
                "                         \"vlan-id\": "+vlanId+"\n" +
                "                     },\n" +
                //"                     \"vlan-pcp\": "+vlanPcp+"\n" +
                "                 }\n" +
                "}";
        return new JSONObject(conf);

    }

    JSONObject vlanPush(String outPort, int vlanId, boolean vlanIdPresent){
        String conf = " {\n" +
                "\"apply-actions\": {" +
                "\"action\": [\n" +
                "                                 {\n" +
                "                                     \"order\": 0,\n" +
                "                                     \"push-vlan-action\": {\n" +
                "                                         \"ethernet-type\": 33024\n" +
                "                                     }\n" +
                "                                 },\n" +
                "                                 {\n" +
                "                                     \"order\": 1,\n" +
                "                                     \"set-field\": {\n" +
                "                                         \"vlan-match\": {\n" +
                "                                             \"vlan-id\": {\n" +
                "                                                 \"vlan-id-present\": "+vlanIdPresent+",\n" +
                "                                                 \"vlan-id\": "+vlanId+"\n" +
                "                                             }\n" +
                "                                         }\n" +
                "                                     }\n" +
                "                                 },\n" +
                "                                 {\n" +
                "                                     \"order\": 2,\n" +
                "                                     \"output-action\": {\n" +
                "                                         \"output-node-connector\": \""+outPort+"\"\n" +
                "                                     }\n" +
                "                                 }\n" +
                "                             ] }\n" +
                "}";
        return new JSONObject(conf);
    }

    JSONObject outPort (String outPort){
        String conf = " { \"apply-actions\": {\n" +
                "                             \"action\": [\n" +
                "                                 {\n" +
                "                                     \"order\": 0,\n" +
                "                                     \"output-action\": {\n" +
                "                                         \"output-node-connector\": \""+outPort+"\"\n" +
                "                                     }\n" +
                "                                 }\n" +
                "                             ]\n" +
                "                         }}";
        return new JSONObject(conf);
    }
}

class MockSdnDriver {

    int interval1 = 1000;
    int interval2 = 2000;
    int interval3 = 3000;

    private Map<String, String> sliceConfig = new HashMap<>();
    MockSdnDriver(){

    }


    void applyConfig(String sliceId, String config) throws InterruptedException {
        Thread.sleep(interval1);
        sliceConfig.put(sliceId, config);
    }

    String getConfig(String sliceId) throws InterruptedException {
        Thread.sleep(interval1);
        return sliceConfig.get(sliceId);
    }


}
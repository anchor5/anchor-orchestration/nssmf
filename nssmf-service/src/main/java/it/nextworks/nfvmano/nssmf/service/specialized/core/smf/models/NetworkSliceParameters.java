package it.nextworks.nfvmano.nssmf.service.specialized.core.smf.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NetworkSliceParameters{
    @JsonProperty("min-DL-rate")
    public int minDLRate;
    @JsonProperty("max-DL-rate")
    public int maxDLRate;
    @JsonProperty("min-UL-rate")
    public int minULRate;
    @JsonProperty("max-UL-rate")
    public int maxULRate;

    @JsonProperty("max-latency")
    public int maxLatency;
    @JsonProperty("max-jitter")
    public int maxJitter;

    public int getMinDLRate() {
        return minDLRate;
    }

    public void setMinDLRate(int minDLRate) {
        this.minDLRate = minDLRate;
    }

    public int getMaxDLRate() {
        return maxDLRate;
    }

    public void setMaxDLRate(int maxDLRate) {
        this.maxDLRate = maxDLRate;
    }

    public int getMinULRate() {
        return minULRate;
    }

    public void setMinULRate(int minULRate) {
        this.minULRate = minULRate;
    }

    public int getMaxULRate() {
        return maxULRate;
    }

    public void setMaxULRate(int maxULRate) {
        this.maxULRate = maxULRate;
    }

    public int getMaxLatency() {
        return maxLatency;
    }

    public void setMaxLatency(int maxLatency) {
        this.maxLatency = maxLatency;
    }

    public int getMaxJitter() {
        return maxJitter;
    }

    public void setMaxJitter(int maxJitter) {
        this.maxJitter = maxJitter;
    }
}
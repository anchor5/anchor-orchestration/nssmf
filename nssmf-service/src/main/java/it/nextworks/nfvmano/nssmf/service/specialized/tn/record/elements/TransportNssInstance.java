package it.nextworks.nfvmano.nssmf.service.specialized.tn.record.elements;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class TransportNssInstance {
    @Id
    @GeneratedValue
    private long id;

    private UUID nssiId;
    private String satelliteNetworkId;
    private int n9VlanId;

    public TransportNssInstance() {
    }

    public TransportNssInstance(UUID nssiId, String satelliteNetworkId, int n9VlanId) {
        this.satelliteNetworkId = satelliteNetworkId;
        this.nssiId= nssiId;
        this.n9VlanId=n9VlanId;
    }

    public int getN9VlanId() {
        return n9VlanId;
    }

    public UUID getNssiId() {
        return nssiId;
    }

    public String getSatelliteNetworkId() {
        return satelliteNetworkId;
    }
}

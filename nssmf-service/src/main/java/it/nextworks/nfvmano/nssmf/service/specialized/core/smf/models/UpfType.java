package it.nextworks.nfvmano.nssmf.service.specialized.core.smf.models;

public enum UpfType {
    I_UPF,
    A_UPF
}

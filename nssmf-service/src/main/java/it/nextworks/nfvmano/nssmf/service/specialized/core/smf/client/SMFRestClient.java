package it.nextworks.nfvmano.nssmf.service.specialized.core.smf.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.nfvmano.libs.vs.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.nssmf.service.nssmanagement.NssLcmEventHandler;
import it.nextworks.nfvmano.nssmf.service.specialized.core.smf.models.SMFConfigRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;



public class SMFRestClient {

    private String baseUrl;
    protected static final Logger log = LoggerFactory.getLogger(SMFRestClient.class);

    public SMFRestClient(String baseUrl){
        this.baseUrl = baseUrl;

    }

    public void configureSMF(SMFConfigRequest smfConfigRequest) throws FailedOperationException {
        log.info("Recevied request to configure SMF");
        RestTemplate restTemplate  = new RestTemplate();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String smfConfigRequestStr = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(smfConfigRequest);
            log.debug(smfConfigRequestStr);
            HttpHeaders httpHeaders = new HttpHeaders();
            HttpEntity<String> httpEntity = new HttpEntity<String>(smfConfigRequestStr,httpHeaders );
            restTemplate.postForObject(baseUrl, httpEntity, String.class );
        } catch (Exception e) {
            log.error("Error during SMF configuration:", e);
            throw new FailedOperationException(e);
        }

    }
}

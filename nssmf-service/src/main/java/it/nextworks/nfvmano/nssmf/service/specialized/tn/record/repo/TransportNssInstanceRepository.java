package it.nextworks.nfvmano.nssmf.service.specialized.tn.record.repo;

import it.nextworks.nfvmano.nssmf.service.specialized.core.record.elements.CoreNssInstance;
import it.nextworks.nfvmano.nssmf.service.specialized.tn.record.elements.TransportNssInstance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface TransportNssInstanceRepository extends JpaRepository<TransportNssInstance, Long> {

    Optional<TransportNssInstance> findByNssiId(UUID nssiId);
}

package it.nextworks.nfvmano.nssmf.service.specialized.core.record.elements;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Map;

@Entity
public class CoreVnfAddressAlloc {

    @Id
    @GeneratedValue
    private long id;

    private String memberVnfIndexRef;

    @ManyToOne
    private CoreNssInstance coreNssInstance;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private Map<CoreNetwork, String> extCpAddress;

    public CoreVnfAddressAlloc() {
    }

    public String getMemberVnfIndexRef() {
        return memberVnfIndexRef;
    }

    public void setMemberVnfIndexRef(String memberVnfIndexRef) {
        this.memberVnfIndexRef = memberVnfIndexRef;
    }

    public CoreNssInstance getCoreNssInstance() {
        return coreNssInstance;
    }

    public void setCoreNssInstance(CoreNssInstance coreNssInstance) {
        this.coreNssInstance = coreNssInstance;
    }

    public Map<CoreNetwork, String> getExtCpAddress() {
        return extCpAddress;
    }

    public void setExtCpAddress(Map<CoreNetwork, String> extCpAddress) {
        this.extCpAddress = extCpAddress;
    }
}

/*
 * Copyright (c) 2022 Nextworks s.r.l.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.nextworks.nfvmano.nssmf.service.specialized.core;

import it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.vs.common.nssmf.messages.specialized.core.UpfProvisioningPayload;
import it.nextworks.nfvmano.nssmf.service.messages.notification.NssiStatusChangeNotiticationMessage;
import it.nextworks.nfvmano.nssmf.service.messages.provisioning.InstantiateNssiRequestMessage;
import it.nextworks.nfvmano.nssmf.service.nssmanagement.NssLcmEventHandler;


import java.util.Map;
import java.util.UUID;

/**
 * Mock Class implementing the steps for UPFs LCM
 *
 * @author Pietro G. Giardina
 */
public class UPFMock extends NssLcmEventHandler {

    private MockOSDriver mockOSDriver = new MockOSDriver();
    private MockOSMDriver osmDriver = new MockOSMDriver();
    public UPFMock(){
        this.setEnableAutoNotification(true);
    }

    @Override
    protected void processInstantiateNssRequest(InstantiateNssiRequestMessage message) throws NotExistingEntityException {
        UpfProvisioningPayload payload = (UpfProvisioningPayload)message.getInstantiateNssiRequest();
        log.info("Instantiating 5GCore network sub-slice. ID: " + this.getNetworkSubSliceInstanceId().toString());
        log.info("Network slice Information: " + payload.getNetworkSliceType() +  payload.getNetworkSliceParamters());
        //Step 1 Get Net info form OpenStack
        log.info("Retrieving Network information from VIM");
        try {


            String net_id = mockOSDriver.retrieveNet(ifaces.N4);
            log.info("N4 FOUND - net_id: " + net_id);

            log.info("Creating N9");
            log.info("N9 Config:");
            log.info(" Shared            Yes\n" +
                    "                  External Network  Yes\n" +
                    "                  MTU               1500\n" +
                    "                  DHCP Agents       NO\n" +
                    "                  Provider Network  \n" +
                    "                      Network Type:     flat\n" +
                    "                      Physical Network: provider_N9\n" +
                    "                  Subnet\n" +
                    "                      CIDR: 10.7.1.0/24\n" +
                    "                      IP Version:   IPv4\n" +
                    "                      Gateway IP:   10.7.1.1\n" +
                    "                      Allocation Pools: 10.7.1.2,10.7.1.254");

            net_id = mockOSDriver.createNet(ifaces.N9);
            log.info("N9_net_" + this.getNetworkSubSliceInstanceId().toString() + " net_id: " + net_id);
            //Step 2 UPF @Core

            Map<String, String> upfs = payload.getUserPlaneFunctions();
            String UpfOsmId = "";
            for(String segment :upfs.keySet()){
                if(segment.equals("UPF-A")){
                    log.info("Creating N6 for A-UPF");
                    net_id = mockOSDriver.createNet(ifaces.N6);
                    log.info("N6_" + this.getNetworkSubSliceInstanceId().toString() + "_A net_id: " + net_id);
                    log.info("Instantiating A-UPF");
                    log.info("Target VIM ID: "+upfs.get(segment));
                    UpfOsmId = osmDriver.instantiateUPF(segment);
                }else{
                    //Step 2 UPF @Edge
                    log.info("Creating N6 for I-UPF");
                    net_id = mockOSDriver.createNet(ifaces.N6);
                    log.info("N6_" + this.getNetworkSubSliceInstanceId().toString() + "_I net_id: " + net_id);
                    log.info("Instantiating I-UPF");
                    log.info("Target VIM ID:" + upfs.get(segment));
                    UpfOsmId = osmDriver.instantiateUPF(segment);

                }
                log.info("Network Service created. ID "+ UpfOsmId);
            }

            //Step 3 configure SMF
            log.info("Configuring  SMF");
            log.info("{\n" +
                    "    \"network-slice-type\": {\n" +
                    "        \"network-slice-type\": \"eMBB\"\n" +
                    "    },\n" +
                    "    \"network-slice-parameters\": {\n" +
                    "        \"min_DL_rate\": "+payload.getNetworkSliceParamters().get("min_DL_rate")+",\n" +
                    "        \"max_DL_rate\": "+payload.getNetworkSliceParamters().get("max_DL_rate")+",\n" +
                    "        \"min_UL_rate\": "+payload.getNetworkSliceParamters().get("min_UL_rate")+",\n" +
                    "        \"max_UL_rate\": "+payload.getNetworkSliceParamters().get("max_UL_rate")+",\n" +
                    "        \"max_latency\": "+payload.getNetworkSliceParamters().get("max_latency")+" ,\n" +
                    "        \"max-jitter\": "+payload.getNetworkSliceParamters().get("max-jitter")+"\n" +
                    "    },\n" +
                    "    \"network-slice-id\": \"" + payload.getE2eSliceId() + "\",\n" +
                    "    \"upfs\": [\n" +
                    "        {\n" +
                    "            \"name\": \"Core UPF\",\n" +
                    "            \"UPF-id\": \"UPF_001_01\",\n" +
                    "            \"upf-type\": \"A_UPF\",\n" +
                    "            \"upf-ip-addresses\": [\n" +
                    "                {\n" +
                    "                    \"network-interface\": \"N9\",\n" +
                    "                    \"ipv4-address\": \"10.7.1.2\"\n" +
                    "                }\n" +
                    "            ]\n" +
                    "        },\n" +
                    "        {\n" +
                    "            \"name\": \"Intermediate UPF\",\n" +
                    "            \"UPF-id\": \"UPF_001_02\",\n" +
                    "            \"upf-type\": \"I_UPF\",\n" +
                    "            \"upf-ip-addresses\": [\n" +
                    "                {\n" +
                    "                    \"network-interface\": \"N9\",\n" +
                    "                    \"ipv4-address\": \"10.7.1.3\"\n" +
                    "                }\n" +
                    "            ]\n" +
                    "        }\n" +
                    "    ]\n" +
                    "}");
            log.info("UPFs instantiation completed");
            NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
            notif.setSuccess(true);
            this.getEventBus().post(notif);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

enum ifaces{
    N4,
    N6,
    N9,
    MGMT
}
class MockOSDriver{
    int interval1 = 1000;
    int interval2 = 2000;
    int interval3 = 3000;

    public String retrieveNet(ifaces iface) throws InterruptedException {
        Thread.sleep(interval1);
        if(iface == ifaces.N4){
            return "923e576b-64e2-4347-8a04-d826824557ee";
        }else
            return "f5f70c74-0260-4905-9a13-1a3e9515d97b";
    }

    public String createNet(ifaces iface) throws InterruptedException {
        Thread.sleep(interval3);
        return UUID.randomUUID().toString();
    }
}

class MockOSMDriver{
    int interval1 = 1000;
    int interval2 = 2000;
    int interval3 = 3000;

    public String instantiateUPF(String nsdId) throws InterruptedException {
        Thread.sleep(interval3);
        return UUID.randomUUID().toString();
    }

}

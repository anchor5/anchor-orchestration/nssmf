package it.nextworks.nfvmano.nssmf.service.specialized.core.record.repo;

import it.nextworks.nfvmano.nssmf.service.specialized.core.record.elements.CoreVnfAddressAlloc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoreVnfAddressAllocRepository extends JpaRepository<CoreVnfAddressAlloc, Long> {
}

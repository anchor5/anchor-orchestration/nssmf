package it.nextworks.nfvmano.nssmf.service.specialized.core;

import it.nextworks.nfvmano.libs.vs.common.nssmf.elements.NssiStatus;
import it.nextworks.nfvmano.nssmf.service.specialized.core.record.elements.CoreNetwork;
import it.nextworks.nfvmano.nssmf.service.specialized.core.record.elements.CoreVnfAddressAlloc;
import it.nextworks.nfvmano.nssmf.service.specialized.core.smf.client.SMFRestClient;
import it.nextworks.nfvmano.nssmf.service.specialized.core.smf.models.*;
import it.nextworks.nfvmano.sbi.nfvo.elements.NfvoDriverType;
import it.nextworks.nfvmano.sbi.nfvo.elements.NfvoInformation;
import it.nextworks.nfvmano.sbi.nfvo.interfaces.NfvoLcmNotificationConsumerInterface;
import it.nextworks.nfvmano.sbi.nfvo.messages.*;
import it.nextworks.nfvmano.sbi.nfvo.osm.Osm10Client;
import it.nextworks.nfvmano.sbi.nfvo.osm.rest.model.NsInstance;
import it.nextworks.nfvmano.sbi.nfvo.osm.rest.model.VnfInstanceInfo;
import it.nextworks.nfvmano.sbi.nfvo.osm.rest.model.VnfInterface;
import it.nextworks.nfvmano.sbi.nfvo.polling.NfvoLcmNotificationsManager;
import it.nextworks.nfvmano.sbi.nfvo.polling.NfvoLcmOperationPollingManager;
import it.nextworks.nfvmano.libs.vs.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.vs.common.nssmf.messages.specialized.core.UpfProvisioningPayload;
import it.nextworks.nfvmano.nssmf.service.messages.notification.NssiStatusChangeNotiticationMessage;
import it.nextworks.nfvmano.nssmf.service.messages.provisioning.InstantiateNssiRequestMessage;
import it.nextworks.nfvmano.nssmf.service.messages.provisioning.TerminateNssiRequestMessage;
import it.nextworks.nfvmano.nssmf.service.nssmanagement.NssLcmEventHandler;
import it.nextworks.nfvmano.nssmf.service.specialized.core.record.elements.CoreNssInstance;
import it.nextworks.nfvmano.nssmf.service.specialized.core.record.service.CoreNssInstanceRecordService;
import org.checkerframework.checker.units.qual.C;
import org.openstack4j.api.Builders;
import org.openstack4j.api.OSClient;
import org.openstack4j.model.common.Identifier;
import org.openstack4j.model.network.IPVersionType;
import org.openstack4j.model.network.Network;
import org.openstack4j.model.network.NetworkType;
import org.openstack4j.model.network.Subnet;
import org.openstack4j.model.network.builder.NetworkBuilder;
import org.openstack4j.model.network.builder.SubnetBuilder;
import org.openstack4j.model.tacker.Vnf;
import org.openstack4j.openstack.OSFactory;

import java.util.*;

/**
 * *
 *  * @author Juan Brenes
 *  Configuration parameters:
 *      - core.n4_network
 *      - core.n3_edge_network
 *      - core.n4_vl  (n4_net)
 *      - core.n9_vl (n9_net)
 *      - core.n9_physical_network
 *      - core.n6_core_vl (n6_core_net)
 *      - core.n6_edge_vl (n6_edge_net)
 *      - core.nsi_name (core_)
 *      - core.upf_i_n3_cp (cp_n3_edge-ext)
 *      - core.upf_i_address
 *      - core.upf_i_ref (UPF_I)
 *      - core.upf_a_cp (cp_n6_core-ext)
 *      - core.upf_i_n9_cp (cp_n9-ext)
 *      - core.upf_a_n9_cp (cp_n9-ext)
 *      - core.upf_i_n4_cp (cp_n4-ext)
 *      - core.upf_a_n4_cp (cp_n4-ext)
 *      - core.upf_a_address
 *      - core.upf_a_ref (UPF_A)
 *      - core.smf.baseurl
 *      - vim.address
 *      - vim.username
 *      - vim.password
 *      - vim.domain
 *      - vim.project
 *      - nssmf.osm.baseurl
 *      - nssmf.osm.username
 *      - nssmf.osm.password
 *      - nssmf.osm.project
 */
public class CoreNSSMF extends NssLcmEventHandler implements NfvoLcmNotificationConsumerInterface {


    private UpfProvisioningPayload payload;


    @Override
    protected void processInstantiateNssRequest(InstantiateNssiRequestMessage message) throws NotExistingEntityException, FailedOperationException {
        payload = (UpfProvisioningPayload) message.getInstantiateNssiRequest();
        log.info("Instantiating 5GCore network sub-slice. ID: " + this.getNetworkSubSliceInstanceId().toString());
        log.info("Network slice Information: " + payload.getNetworkSliceType() + payload.getNetworkSliceParamters());
        //Step 1 Get Net info form OpenStack
        log.info("Retrieving Network information from VIM");

        String n4NetworkName = getEnvironment().getProperty("core.n4_network", "n4_network");
        String n4Id = this.retrieveOpenStackNetworkId(n4NetworkName);


        String n3EdgeNetworkName = getEnvironment().getProperty("core.n3_edge_network", "n3_network");
        String n3EdgeNetworkId = this.retrieveOpenStackNetworkId(n3EdgeNetworkName);

        String n6CoreNetworkName = getEnvironment().getProperty("core.n6_core_network", "n6_network");
        String n6CoreNetworkId = this.retrieveOpenStackNetworkId(n6CoreNetworkName);
        log.debug("Retrieved IDS N4:"+n4Id+" N3:"+n3EdgeNetworkId+" N6:"+n6CoreNetworkId);
        log.info("Allocating VIM networks");
        //N9 Creation
        log.debug("Creating N9 Network");
        String n9NetworkName = getEnvironment().getProperty("core.n9_vl", "n9_net");
        String n9PhysicalNetwork = getEnvironment().getProperty("core.n9_physical_network", "provider_N9");
        String n9Segmentation = getN9Segmententation();
        String n9NetworkId = this.createOpenStackNetwork(n9NetworkName, true, NetworkType.VLAN, n9Segmentation, n9PhysicalNetwork, Boolean.TRUE);
        String n9SubnetId= createOpenStackSubnet(n9NetworkId, true, getN9SubnetCidr());
        log.debug("Created N9 Network: "+n9NetworkId+ " - Name:"+n9NetworkName+"\tPhysical network:"+n9PhysicalNetwork+"\tVLAN:"+n9Segmentation);

        //N6 Creation
        log.debug("Creating N6 Network");
        //String n6CoreNetworkName = getEnvironment().getProperty("core.n6_core_vl", "n6_core_net");
        String n6CoreVlName = getEnvironment().getProperty("core.n6_core_vl", "n6_core_net");
        //String n6EdgeNetworkName = getEnvironment().getProperty("core.n6_edge_vl", "n6_edge_net");

        //String n6CoreNetworkId = this.createOpenStackNetwork(n6CoreNetworkName, true, null, null, null, null);
        //String n6CoreSubnetId = this.createOpenStackSubnet(n6CoreNetworkId, true, getN6CoreSubnetCidr());

        //String n6EdgeNetworkId = this.createOpenStackNetwork(n6EdgeNetworkName, true, null, null, null, null);
        //String n6EdgeSubnetId = this.createOpenStackSubnet(n6EdgeNetworkId, true, getN6EdgeSubnetCidr());
        //log.debug("Created N6 Core Network: "+n6CoreNetworkId+ " - Name:"+n6CoreNetworkName);
        //log.debug("Created N6 Edge Network: "+n6EdgeNetworkId+ " - Name:"+n6EdgeNetworkName);
        //N3 Creation
        //log.debug("Creating N6 Network");
        //String n3EdgeNetworkName = getEnvironment().getProperty("core.n3_edge_vl", "n3_edge_net");
        //String n3EdgeNetworkId = this.createOpenStackNetwork(n3EdgeNetworkName, true, null, null, null, null);
        //String n3EdgeSubnetId = this.createOpenStackSubnet(n6EdgeNetworkId, true, getN6EdgeSubnetCidr());

        //log.debug("Created N3 Edge Network: "+n3EdgeNetworkId+ " - Name:"+n3EdgeNetworkName);
        log.info("Creating NS identifier");
        Osm10Client nfvoClient = getOsmClient();
        String nsInstanceName = getEnvironment().getProperty("core.nsi_name", "core_ns")+"_"+this.getNetworkSubSliceInstanceId();
        String nsInstanceId = nfvoClient.createNetworkServiceIdentifier(
                    new CreateNsIdentifierRequestInternal(payload.getNsdId(), nsInstanceName));
        NfvoLcmNotificationsManager notificationsManager = (NfvoLcmNotificationsManager)getDriverFactory().getDriver("nfvoLcmNotificationsManager");
        notificationsManager.registerNotificationConsumer(nsInstanceId, this);

        log.info("Triggering NS instantiation operation for: "+nsInstanceId);
        InstantiateNsRequestInternal instantiateNsRequestInternal =
                new InstantiateNsRequestInternal(nsInstanceName, payload.getNsdId(), nsInstanceId, null);
        List<VlAllocation> vlAllocations = new ArrayList<>();
        vlAllocations.add(getN9VlAllocation(payload.getUserPlaneFunctions()));
        vlAllocations.add(getN4VlAllocation(payload.getUserPlaneFunctions()));
        vlAllocations.add(getN6CoreVlAllocation(payload.getUserPlaneFunctions()));
        //vlAllocations.add(getN6EdgeVlAllocation(payload.getUserPlaneFunctions()));
        vlAllocations.add(getN3EdgeVlAllocation(payload.getUserPlaneFunctions()));
        List<VnfAllocation> vnfAllocations = new ArrayList<>();
        for(String upf : payload.getUserPlaneFunctions().keySet()){
            String vimId = payload.getUserPlaneFunctions().get(upf);
            vnfAllocations.add(new VnfAllocation(upf, UUID.fromString(vimId)));

        }

        instantiateNsRequestInternal.setVlAllocations(vlAllocations);
        instantiateNsRequestInternal.setVnfAllocations(vnfAllocations);
        String nsOperationId = nfvoClient.instantiateNetworkService(instantiateNsRequestInternal);
        log.debug("Received operation id:"+nsOperationId);
        CoreNssInstanceRecordService service = (CoreNssInstanceRecordService) getRecordServiceFactory().getRecordService("coreNssInstanceRecordService");

        NetworkSliceType sliceType = NetworkSliceType.valueOf(payload.getNetworkSliceType());
        service.createCoreNssInstanceRecord(new CoreNssInstance(
                this.getNetworkSubSliceInstanceId(),
                n6CoreNetworkId,
                //n6CoreSubnetId,
                null,
                n9NetworkId,
                n9SubnetId,
                //n6EdgeNetworkId,
                null,
                //n6EdgeSubnetId,
                null,
                n3EdgeNetworkId,
                //n3EdgeSubnetId,
                null,
                UUID.fromString(nsInstanceId),
                sliceType));

    }


    private VlAllocation getN4VlAllocation(Map<String,String> upfMapping){
        String n4NetworkName = getEnvironment().getProperty("core.n4_network", "n4_network");
        String n4Vl = getEnvironment().getProperty("core.n4_vl", "n4_net");
        VlAllocation n4Allocation = new VlAllocation(n4Vl, new HashMap<>(), new ArrayList<>());
        for(String upf : upfMapping.keySet()){
            String vimId = upfMapping.get(upf);

            n4Allocation.addVimNetworkName(vimId, n4NetworkName);
        }
        return n4Allocation;
    }

    private VlAllocation getN9VlAllocation(Map<String,String> upfMapping){
        String n9Vl = getEnvironment().getProperty("core.n9_vl", "n9_net");
        String n9NetworkName = getNetworkNameForVl(n9Vl);

        //String upfAVnfRef = getEnvironment().getProperty("core.upf_a_ref", "UPF_A");
        //String upfAN9Cp = getEnvironment().getProperty("core.upf_a_n9_cp", "cp_n9_core-ext");
        //String upfAN9Address = getEnvironment().getProperty("core.upf_a_n9_address");

        //String upfIVnfRef = getEnvironment().getProperty("core.upf_i_ref", "UPF_I");
        //String upfIN9Cp = getEnvironment().getProperty("core.upf_i_n9_cp", "cp_n9_core-ext");
        //String upfIN9Address = getEnvironment().getProperty("core.upf_i_n9_address");

        List<VnfConnectionPointAllocation> cpAllocation = new ArrayList<>();
        //cpAllocation.add(new VnfConnectionPointAllocation(upfAVnfRef, upfAN9Cp, upfAN9Address));
        //cpAllocation.add(new VnfConnectionPointAllocation(upfIVnfRef, upfIN9Cp, upfIN9Address));
        VlAllocation n9Allocation = new VlAllocation(n9Vl, new HashMap<>(), cpAllocation);
        for(String upf : upfMapping.keySet()){
            String vimId = upfMapping.get(upf);

            n9Allocation.addVimNetworkName(vimId, n9NetworkName);
        }
        return n9Allocation;
    }

    private VlAllocation getN6CoreVlAllocation(Map<String,String> upfMapping){
        String n6CoreNetworkVl = getEnvironment().getProperty("core.n6_core_vl", "n6_core_net");
        //String n6CoreNetworkName = getNetworkNameForVl(n6CoreNetworkVl);
        String n6CoreNetworkName = getEnvironment().getProperty("core.n6_core_network", "n6_network");
        //String upfAVnfRef = getEnvironment().getProperty("core.upf_a_ref", "UPF_A");
        //String upfACp = getEnvironment().getProperty("core.upf_a_cp", "cp_n6_core-ext");
        //String upfAAddress = getEnvironment().getProperty("core.upf_a_address");
        //List<VnfConnectionPointAllocation> cpAllocation = new ArrayList<>();
        //cpAllocation.add(new VnfConnectionPointAllocation(upfAVnfRef, upfACp, upfAAddress));
        VlAllocation n6Allocation = new VlAllocation(n6CoreNetworkVl, new HashMap<>(), new ArrayList<>());
        for(String upf : upfMapping.keySet()){
            String vimId = upfMapping.get(upf);

            n6Allocation.addVimNetworkName(vimId, n6CoreNetworkName);
        }
        return n6Allocation;
    }

    private VlAllocation getN6EdgeVlAllocation(Map<String,String> upfMapping){
        String n6EdgeNetworkVl = getEnvironment().getProperty("core.n6_edge_vl", "n6_edge_net");
        String n6EdgeNetworkName = getNetworkNameForVl(n6EdgeNetworkVl);

        //String upfIVnfRef = getEnvironment().getProperty("core.upf_i_ref", "UPF_I");
        //String upfICp = getEnvironment().getProperty("core.upf_i_cp", "cp_n6_edge-ext");
        //String upfIAddress = getEnvironment().getProperty("core.upf_i_address");
        //List<VnfConnectionPointAllocation> cpAllocation = new ArrayList<>();
        //cpAllocation.add(new VnfConnectionPointAllocation(upfIVnfRef, upfICp, upfIAddress));
        VlAllocation n6EdgeAllocation = new VlAllocation(n6EdgeNetworkVl, new HashMap<>(), new ArrayList<>());
        for(String upf : upfMapping.keySet()){
            String vimId = upfMapping.get(upf);

            n6EdgeAllocation.addVimNetworkName(vimId, n6EdgeNetworkName);
        }
        return n6EdgeAllocation;
    }

    private VlAllocation getN3EdgeVlAllocation(Map<String, String> upfMapping){
        String n3EdgeNetworkVl = getEnvironment().getProperty("core.n3_edge_vl", "n3_edge_net");
        //String n3EdgeNetworkName = getNetworkNameForVl(n3EdgeNetworkVl);
        String n3EdgeNetworkName = getEnvironment().getProperty("core.n3_edge_network", "n3_network");
        String upfIVnfRef = getEnvironment().getProperty("core.upf_i_ref", "UPF_I");
        String upfICp = getEnvironment().getProperty("core.upf_i_cp", "cp_n3_edge-ext");
        String upfIAddress = getEnvironment().getProperty("core.upf_i_address");
        List<VnfConnectionPointAllocation> cpAllocation = new ArrayList<>();
        cpAllocation.add(new VnfConnectionPointAllocation(upfIVnfRef, upfICp, upfIAddress));
        VlAllocation n3EdgeAllocation = new VlAllocation(n3EdgeNetworkVl, new HashMap<>(), cpAllocation);
        for(String upf : upfMapping.keySet()){
            String vimId = upfMapping.get(upf);

            n3EdgeAllocation.addVimNetworkName(vimId, n3EdgeNetworkName);
        }
        return n3EdgeAllocation;
    }


    @Override
    protected void processTerminateNssRequest(TerminateNssiRequestMessage message) throws NotExistingEntityException, FailedOperationException {
        log.info("Terminating NS instance");
        Osm10Client nfvoClient = getOsmClient();
        CoreNssInstanceRecordService service = (CoreNssInstanceRecordService) getRecordServiceFactory().getRecordService("coreNssInstanceRecordService");
        CoreNssInstance instance = service.getCoreNssInstance(this.getNetworkSubSliceInstanceId());
        //Just in case we register again to receive the notification
        NfvoLcmNotificationsManager notificationsManager = (NfvoLcmNotificationsManager)getDriverFactory().getDriver("nfvoLcmNotificationsManager");
        notificationsManager.registerNotificationConsumer(instance.getNetworkServiceInstanceId().toString(), this);
        String terminateOpId = nfvoClient.terminateNetworkService(instance.getNetworkServiceInstanceId().toString());


    }

    private void deleteOpenStackNetworks() throws FailedOperationException, NotExistingEntityException {
        CoreNssInstanceRecordService service = (CoreNssInstanceRecordService) getRecordServiceFactory().getRecordService("coreNssInstanceRecordService");
        CoreNssInstance instance = service.getCoreNssInstance(this.getNetworkSubSliceInstanceId());

        //N3 Edge
        //this.deleteOpenStackSubnet(instance.getN3EdgeSubnetId());
        //this.deleteOpenStackNetwork(instance.getN3EdgeNetworkId());

        //N6 Edge
        //this.deleteOpenStackSubnet(instance.getN6EdgeSubnetId());
        //this.deleteOpenStackNetwork(instance.getN6EdgeNetworkId());

        //N6 Core
        //this.deleteOpenStackSubnet(instance.getN6CoreSubnetId());
        //this.deleteOpenStackNetwork(instance.getN6CoreNetworkId());

        //N9
        this.deleteOpenStackSubnet(instance.getN9SubnetId());
        this.deleteOpenStackNetwork(instance.getN9NetworkId());

        NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
        notif.setSuccess(true);
        this.getEventBus().post(notif);

    }

    private String getNetworkNameForVl(String vl){
        return vl+"_"+this.getNetworkSubSliceInstanceId().toString();
    }

    private String retrieveOpenStackNetworkId(String networkName) throws FailedOperationException {
        log.debug("Retrieving Openstack Network ID for: "+ networkName);
        OSClient.OSClientV3 os = getOpenStackClient();
        log.debug("Successfully authenticated with VIM");
        List<? extends Network> networks = os.networking().network().list();
        Optional<? extends Network> targetNetwork = networks.stream().filter(n -> n.getName().equals(networkName))
                .findFirst();
        if(targetNetwork.isPresent()){
            log.debug("Retrieved network information for: "+networkName+ " - "+targetNetwork.get().getId());
            return targetNetwork.get().getId();
        }else throw new FailedOperationException("Could not find VIM network with name:"+networkName);



    }

    private Network retrieveOpenStackNetworkById(String networkId) throws FailedOperationException {
        log.debug("Retrieving Openstack Network: "+ networkId);
        OSClient.OSClientV3 os = getOpenStackClient();
        log.debug("Successfully authenticated with VIM");
        List<? extends Network> networks = os.networking().network().list();
        Optional<? extends Network> targetNetwork = networks.stream().filter(n -> n.getId().equals(networkId))
                .findFirst();
        if(targetNetwork.isPresent()){
            log.debug("Retrieved network: "+targetNetwork.get().getId());
            return targetNetwork.get();
        }else throw new FailedOperationException("Could not find VIM network with ID:"+networkId);



    }


    private void deleteOpenStackSubnet(String subnetId) throws FailedOperationException {
        log.debug("Deleting Openstack Subnet: "+ subnetId);
        OSClient.OSClientV3 os = getOpenStackClient();
        os.networking().subnet().delete(subnetId);
    }

    private void deleteOpenStackNetwork(String networkId) throws FailedOperationException {
        log.debug("Deleting Openstack Network: "+ networkId);
        OSClient.OSClientV3 os = getOpenStackClient();
        os.networking().network().delete(networkId);
    }

    private String createOpenStackNetwork(String networkName, boolean isShared, NetworkType networkType, String segmentId, String physicalNetwork, Boolean externalRouter) throws FailedOperationException {
        log.debug("Creating Openstack Network: "+ networkName);
        OSClient.OSClientV3 os = getOpenStackClient();
        String sliceNetworkName = networkName+"_"+this.getNetworkSubSliceInstanceId().toString();
        NetworkBuilder builder = Builders.network().name(sliceNetworkName)
                .isShared(isShared)
                .adminStateUp(true);


        if(networkType!=null)
            builder.networkType(networkType);
        if(segmentId != null)
            builder.segmentId(segmentId);
        if(physicalNetwork!=null)
            builder.physicalNetwork(physicalNetwork);
        if(externalRouter!=null){
            builder.isRouterExternal(externalRouter.booleanValue());
        }

        Network network = os.networking().network().create(builder.build());
        log.debug("Successfully created OpenStack network: "+sliceNetworkName + " - "+network.getId());
        return network.getId();
    }

    private String createOpenStackSubnet(String networkId, boolean enableDhcp, String cidr) throws  FailedOperationException{
        log.debug("Creating Openstack Subnet for network: "+ networkId);
        OSClient.OSClientV3 os = getOpenStackClient();
        Network network = retrieveOpenStackNetworkById(networkId);
        SubnetBuilder builder = Builders.subnet()
                .network(network)
                .cidr(cidr)
                .enableDHCP(enableDhcp)
                .ipVersion(IPVersionType.V4);

        Subnet subnet = os.networking().subnet().create(builder.build());
        log.debug("Successfully created OpenStack subnet: "+network.getId());
        return subnet.getId();
    }

    private String getN9Segmententation(){
        return getEnvironment().getProperty("core.n9_segmentation", "20");
    }

    private String getN9SubnetCidr(){
        return getEnvironment().getProperty("core.n9_cidr", "172.17.3.0/24");
    }

    private String getN6CoreSubnetCidr(){
        return getEnvironment().getProperty("core.n6_core_cidr", "172.17.1.0/24");
    }

    private String getN6EdgeSubnetCidr(){
        return getEnvironment().getProperty("core.n6_edge_cidr", "172.17.2.0/24");
    }

    private OSClient.OSClientV3 getOpenStackClient() throws FailedOperationException {
        log.debug("Creating OpenStack Client");
        try{
            String address  = getEnvironment().getProperty("vim.address");
            String username  = getEnvironment().getProperty("vim.username");
            String password = getEnvironment().getProperty("vim.password");
            String project = getEnvironment().getProperty("vim.project");
            String domain = getEnvironment().getProperty("vim.domain", "default");
            boolean debugVim = Boolean.parseBoolean(getEnvironment().getProperty("vim.debug", "false"));
            OSFactory.enableHttpLoggingFilter(debugVim);
            Identifier domainIdentifier = Identifier.byName(domain);
            Identifier projectIdentifier = Identifier.byName(project);

            OSClient.OSClientV3 os = OSFactory.builderV3()
                    .endpoint(address)
                    .credentials(username, password, domainIdentifier)
                    .scopeToProject(projectIdentifier, domainIdentifier)

                    .authenticate();
            log.debug("Successfully authenticated OpenStack client");
            return os;
        }catch(Exception e){
            log.error("Error while interacting with OpenStack:",e);
            throw new FailedOperationException(e.getMessage());

        }

    }

    private Osm10Client getOsmClient(){
        String ipOsm = getEnvironment().getProperty("nssmf.osm.baseurl");
        String usernameOsm = getEnvironment().getProperty("nssmf.osm.username");
        String passwordOsm = getEnvironment().getProperty("nssmf.osm.password");
        String projectOsm = getEnvironment().getProperty("nssmf.osm.project");
        String UUID_VIM_ACCOUNT = getEnvironment().getProperty("nssmf.osm.vim-id");
        NfvoLcmOperationPollingManager nfvoLcmOperationPollingManager = (NfvoLcmOperationPollingManager) getDriverFactory().getDriver("nfvoLcmOperationPollingManager");


        NfvoInformation info = new NfvoInformation(ipOsm, UUID.fromString(UUID_VIM_ACCOUNT), usernameOsm, passwordOsm, NfvoDriverType.OSM10, projectOsm, "VM_MGMT");
        return new Osm10Client(info, nfvoLcmOperationPollingManager);
    }

    private SMFRestClient getSMFRestClient(){
        String smfAddress = getEnvironment().getProperty("core.smf.baseurl");
        return new SMFRestClient(smfAddress);
    }

    private void configureSMF() throws NotExistingEntityException, FailedOperationException {
        log.info("Triggering SMF configuration");
        CoreNssInstanceRecordService service = (CoreNssInstanceRecordService) getRecordServiceFactory().getRecordService("coreNssInstanceRecordService");
        CoreNssInstance instance = service.getCoreNssInstance(this.getNetworkSubSliceInstanceId());
        SMFRestClient smfRestClient = getSMFRestClient();
        SMFConfigRequest smfConfigRequest = new SMFConfigRequest();
        smfConfigRequest.setNetworkSliceId(this.getNetworkSubSliceInstanceId().toString());
        smfConfigRequest.setNetworkSliceType(new NetworkSliceTypeObject(instance.getNetworkSliceType().toString()));
        NetworkSliceParameters sliceParams = new NetworkSliceParameters();
        /*
              min_DL_rate": 83,
             "max_DL_rate": 6,
             "min_UL_rate": 2,
             "max_UL_rate": 23,
             "max_latency": 99,
             "max-jitter": 26
         */
        sliceParams.setMaxDLRate(payload.getNetworkSliceParamters().get("max_DL_rate"));
        sliceParams.setMinDLRate(payload.getNetworkSliceParamters().get("min_DL_rate"));
        sliceParams.setMinULRate(payload.getNetworkSliceParamters().get("min_UL_rate"));
        sliceParams.setMaxULRate(payload.getNetworkSliceParamters().get("max_UL_rate"));
        sliceParams.setMaxJitter(payload.getNetworkSliceParamters().get("max-jitter"));
        sliceParams.setMaxLatency(payload.getNetworkSliceParamters().get("max_latency"));
        log.debug("SMF Configuration parameters:"+payload.getNetworkSliceParamters());
        smfConfigRequest.setNetworkSliceParameters(sliceParams);

        ArrayList<Upf> upfs = new ArrayList<>();
        log.debug("Generating SMF UPF configuration parameters");
        Upf upfA = generateSmfUpfConfig(instance, UpfType.A_UPF, getEnvironment().getProperty("core.upf_a_ref", "UPF_A"));
        Upf upfI = generateSmfUpfConfig(instance, UpfType.I_UPF, getEnvironment().getProperty("core.upf_i_ref", "UPF_I"));
        upfs.add(upfA);
        upfs.add(upfI);
        smfConfigRequest.setUpfs(upfs);
        smfRestClient.configureSMF(smfConfigRequest);
        log.debug("SMF configured");
    }

    private Upf generateSmfUpfConfig(CoreNssInstance instance, UpfType type, String upfRef ){
        Upf upf =  new Upf();
        upf.setName(upfRef+"_"+this.getNetworkSubSliceInstanceId().toString());
        upf.setuPFId(upfRef+"_"+this.getNetworkSubSliceInstanceId().toString());
        upf.setUpfType(type);



        ArrayList<UpfIpAddress> upfAaddresses = new ArrayList<>();
        CoreVnfAddressAlloc alloc = getVnfAddressAllocForVnf(instance, upfRef);
        for(CoreNetwork network: alloc.getExtCpAddress().keySet()){
            if(network == CoreNetwork.N4 || network==CoreNetwork.N6 || network==CoreNetwork.N9|| network==CoreNetwork.N3){
                UpfIpAddress upfAddress = new UpfIpAddress();
                upfAddress.setIpv4Address(alloc.getExtCpAddress().get(network));
                upfAddress.setNetworkInterface(UpfInterface.valueOf(network.toString()));
                upfAaddresses.add(upfAddress);
            }

        }

        upf.setUpfIpAddresses(upfAaddresses);
        return upf;
    }

    private CoreVnfAddressAlloc getVnfAddressAllocForVnf(CoreNssInstance instance, String memberVnfRef ){
        Optional<CoreVnfAddressAlloc> found = instance.getCoreVnfAddressAllocs().stream().filter(alloc -> alloc.getMemberVnfIndexRef().equals(memberVnfRef))
                .findFirst();
        return  found.get();
    }

    private void retrieveNsAddresses() throws NotExistingEntityException, FailedOperationException {
        CoreNssInstanceRecordService service = (CoreNssInstanceRecordService) getRecordServiceFactory().getRecordService("coreNssInstanceRecordService");
        CoreNssInstance instance = service.getCoreNssInstance(this.getNetworkSubSliceInstanceId());
        Osm10Client osm10Client = getOsmClient();
        NsInstance nsInstance = osm10Client.getNsInstance(instance.getNetworkServiceInstanceId().toString());
        String upfAn4CP = getEnvironment().getProperty("core.upf_a_n4_cp", "cp_n4-ext");
        String upfIn4CP = getEnvironment().getProperty("core.upf_i_n4_cp", "cp_n4-ext");
        String upfAn9CP = getEnvironment().getProperty("core.upf_a_n9_cp", "cp_n9-ext");
        String upfIn9CP = getEnvironment().getProperty("core.upf_i_n9_cp", "cp_n9-ext");
        String upfAn6CP = getEnvironment().getProperty("core.upf_a_n6_cp", "cp_n6_core-ext");
        String upfIn6CP = getEnvironment().getProperty("core.upf_i_n6_cp", "cp_n6_edge-ext");
        String upfIn3CP = getEnvironment().getProperty("core.upf_i_n3_cp", "cp_n3_edge-ext");


        for (String vnfInstanceRecord : nsInstance.getConstituentVnfrRef()) {
            VnfInstanceInfo vnfInstanceInfo = osm10Client.getVnfInstanceInfo(vnfInstanceRecord);
            log.debug("Processing address data for:" + vnfInstanceInfo.getMemberVnfIndexRef());
            Map<CoreNetwork, String> extCpAlloct = new HashMap<>();


            for (VnfInterface vnfInterface : vnfInstanceInfo.getVdur().get(0).getInterfaces()) {
                CoreNetwork network = CoreNetwork.OTHER;
                String extCp = vnfInterface.getExternalConnectionPointRef();
                if (extCp.equals(upfAn4CP) || extCp.equals(upfIn4CP)) {
                    network = CoreNetwork.N4;
                } else if (extCp.equals(upfAn6CP) || extCp.equals(upfIn6CP)) {
                    network = CoreNetwork.N6;
                } else if (extCp.equals(upfIn3CP)) {
                    network = CoreNetwork.N3;
                } else if (extCp.equals(upfAn9CP) || extCp.equals(upfIn9CP)) {
                    network =CoreNetwork.N9;
                }
                log.info("Retrieved address association: "+vnfInstanceInfo.getMemberVnfIndexRef()+" network:"+network+" address:"+vnfInterface.getIpAddress());
                extCpAlloct.put(network, vnfInterface.getIpAddress());
                CoreVnfAddressAlloc addressAlloc = new CoreVnfAddressAlloc();
                addressAlloc.setExtCpAddress(extCpAlloct);
                addressAlloc.setCoreNssInstance(instance);
                addressAlloc.setMemberVnfIndexRef(vnfInstanceInfo.getMemberVnfIndexRef());
                service.addCoreVnfAddressAlloc(instance, addressAlloc);
            }

        }
    }

    @Override
    public void notifyNfvNsStatusChange(String nfvNsId, NetworkServiceStatusChange changeType, boolean b) {
        log.info("Received Network Service Status change notification");
        NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
        if(b){
            if(getNssiStatus().equals(NssiStatus.INSTANTIATING)){
                try {
                    this.retrieveNsAddresses();
                    this.configureSMF();
                    notif.setSuccess(true);
                    this.getEventBus().post(notif);
                } catch (NotExistingEntityException | FailedOperationException e) {
                  log.error("Failed to configure SMF. Error message:", e);
                    notif.setSuccess(false);
                    this.getEventBus().post(notif);
                }

            }else if(getNssiStatus().equals(NssiStatus.TERMINATING)){
                try {
                    this.deleteOpenStackNetworks();
                } catch (FailedOperationException | NotExistingEntityException e) {
                    log.error("Error while terminating NSS:",e );
                    notif.setSuccess(false);
                    this.getEventBus().post(notif);
                }
            }

        }else{

            notif.setSuccess(false);
            this.getEventBus().post(notif);
        }



    }
}

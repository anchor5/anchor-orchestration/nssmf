package it.nextworks.nfvmano.nssmf.service.specialized.core.record.service;

import it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.nssmf.service.NssmfLcmService;
import it.nextworks.nfvmano.nssmf.service.specialized.core.record.elements.CoreNssInstance;
import it.nextworks.nfvmano.nssmf.service.specialized.core.record.elements.CoreVnfAddressAlloc;
import it.nextworks.nfvmano.nssmf.service.specialized.core.record.repo.CoreNssInstanceRepository;
import it.nextworks.nfvmano.nssmf.service.specialized.core.record.repo.CoreVnfAddressAllocRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class CoreNssInstanceRecordService {

    private static final Logger log = LoggerFactory.getLogger(CoreNssInstanceRecordService.class);

    @Autowired
    private CoreNssInstanceRepository coreNssInstanceRepository;

    @Autowired
    private CoreVnfAddressAllocRepository coreVnfAddressAllocRepository;

    public CoreNssInstance getCoreNssInstance(UUID nssiId) throws NotExistingEntityException {
        log.info("Retrieving Core NSSI: "+nssiId);
        Optional<CoreNssInstance> coreNssi = coreNssInstanceRepository.findByNssiId(nssiId);
        if(coreNssi.isPresent()){
            return coreNssi.get();
        }else{
            log.warn("Core NSSI with ID: "+nssiId+" NOT FOUND. ");
            throw  new NotExistingEntityException("Core NSSI "+nssiId+" NOT found");
        }


    }

    public void createCoreNssInstanceRecord(CoreNssInstance coreNssInstance){
        log.info("Creating Core NSSI Record");
        coreNssInstanceRepository.saveAndFlush(coreNssInstance);
    }

    public void addCoreVnfAddressAlloc(CoreNssInstance coreNssInstance, CoreVnfAddressAlloc vnfAddressAlloc){
        log.info("Adding Core VNF address allocation");
        coreVnfAddressAllocRepository.saveAndFlush(vnfAddressAlloc);
        coreNssInstance.addCoreVnfAddressAlloc(vnfAddressAlloc);
        coreNssInstanceRepository.saveAndFlush(coreNssInstance);
    }




}

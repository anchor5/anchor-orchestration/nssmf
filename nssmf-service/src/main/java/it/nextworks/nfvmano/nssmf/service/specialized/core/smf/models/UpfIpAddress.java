package it.nextworks.nfvmano.nssmf.service.specialized.core.smf.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpfIpAddress{
    @JsonProperty("network-interface")
    public UpfInterface networkInterface;
    @JsonProperty("ipv4-address")
    public String ipv4Address;

    public UpfInterface getNetworkInterface() {
        return networkInterface;
    }

    public void setNetworkInterface(UpfInterface networkInterface) {
        this.networkInterface = networkInterface;
    }

    public String getIpv4Address() {
        return ipv4Address;
    }

    public void setIpv4Address(String ipv4Address) {
        this.ipv4Address = ipv4Address;
    }
}

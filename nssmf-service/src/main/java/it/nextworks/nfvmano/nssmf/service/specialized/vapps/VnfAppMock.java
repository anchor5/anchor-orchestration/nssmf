/*
 * Copyright (c) 2022 Nextworks s.r.l.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.nextworks.nfvmano.nssmf.service.specialized.vapps;

import it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.vs.common.nssmf.messages.specialized.vnfapps.VnfProvisioningPayload;
import it.nextworks.nfvmano.nssmf.service.messages.notification.NssiStatusChangeNotiticationMessage;
import it.nextworks.nfvmano.nssmf.service.messages.provisioning.InstantiateNssiRequestMessage;
import it.nextworks.nfvmano.nssmf.service.nssmanagement.NssLcmEventHandler;


import java.util.Map;
import java.util.UUID;

public class VnfAppMock extends NssLcmEventHandler {
    private MockOSDriver mockOSDriver = new MockOSDriver();
    private MockOSMDriver osmDriver = new MockOSMDriver();

    public VnfAppMock(){
        this.setEnableAutoNotification(true);
    }


    @Override
    protected void processInstantiateNssRequest(InstantiateNssiRequestMessage message) throws NotExistingEntityException {
        VnfProvisioningPayload payload = (VnfProvisioningPayload)message.getInstantiateNssiRequest();
        log.info("Instantiating VNF APPs sub-slice. ID: " + this.getNetworkSubSliceInstanceId().toString());
        log.info("NSD_ID: " + payload.getNsdId());
        log.info("Placement information: " + payload.getVnfApps());
        //Step 1 Get Net info form OpenStack
        log.info("Retrieving Network information from VIM");

        Map<String, String> apps = payload.getVnfApps();
        String UpfOsmId = "";
        for(String segment :apps.keySet()) {
            if (segment.equals("core")) {
                log.info("Instantiating CORE VNF Application - NSD ID: " + payload.getNsdId());
                log.info("Target VIM ID: " + apps.get(segment));
                log.info("Attaching to network: N6_" + this.getNetworkSubSliceInstanceId().toString() + "_A");
                UpfOsmId = osmDriver.instantiateApp(segment);
            } else {

                log.info("Instantiating EDGE VNF Application - NSD ID: " + payload.getNsdId());
                log.info("Target VIM ID: " + apps.get(segment));
                log.info("Attaching to network: N6_" + this.getNetworkSubSliceInstanceId().toString() + "_I");
                UpfOsmId = osmDriver.instantiateApp(segment);
            }
        }
        log.info("Network Service created. ID " + UpfOsmId);
        NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
        notif.setSuccess(true);
        this.getEventBus().post(notif);

    }
}

enum Ifaces {
    N4,
    N6,
    N9,
    MGMT
}
class MockOSDriver{
    int interval1 = 1000;
    int interval2 = 2000;
    int interval3 = 3000;

    public String retrieveNet(Ifaces iface) throws InterruptedException {
        Thread.sleep(interval1);
        if(iface == Ifaces.N4){
            return "923e576b-64e2-4347-8a04-d826824557ee";
        }else
            return "f5f70c74-0260-4905-9a13-1a3e9515d97b";
    }

    public String createNet(Ifaces iface) throws InterruptedException {
        Thread.sleep(interval3);
        return UUID.randomUUID().toString();
    }
}

class MockOSMDriver{
    int interval1 = 1000;
    int interval2 = 2000;
    int interval3 = 3000;

    public String instantiateApp(String nsdId) {
        try{
            Thread.sleep(interval3);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return UUID.randomUUID().toString();
    }

}
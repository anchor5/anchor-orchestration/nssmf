package it.nextworks.nfvmano.nssmf.service.specialized.core.smf.models;

public enum UpfInterface {
    N4,
    N6,
    N9,
    N3
}

package it.nextworks.nfvmano.nssmf.service.specialized.core.smf.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Upf{
    public String name;
    @JsonProperty("UPF-id")
    public String uPFId;
    @JsonProperty("upf-type")
    public UpfType upfType;
    @JsonProperty("upf-ip-addresses")
    public ArrayList<UpfIpAddress> upfIpAddresses;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getuPFId() {
        return uPFId;
    }

    public void setuPFId(String uPFId) {
        this.uPFId = uPFId;
    }

    public UpfType getUpfType() {
        return upfType;
    }

    public void setUpfType(UpfType upfType) {
        this.upfType = upfType;
    }

    public ArrayList<UpfIpAddress> getUpfIpAddresses() {
        return upfIpAddresses;
    }

    public void setUpfIpAddresses(ArrayList<UpfIpAddress> upfIpAddresses) {
        this.upfIpAddresses = upfIpAddresses;
    }
}

package it.nextworks.nfvmano.nssmf.service.specialized.tn;

import it.nextworks.nfvmano.libs.vs.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.vs.common.nsmf.elements.TransportConfig;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.configuration.ConfigurationActionType;
import it.nextworks.nfvmano.libs.vs.common.nssmf.messages.specialized.transport.SdnConfigPayload;
import it.nextworks.nfvmano.libs.vs.common.ra.elements.TransportFlowType;
import it.nextworks.nfvmano.nssmf.service.messages.notification.NssiStatusChangeNotiticationMessage;
import it.nextworks.nfvmano.nssmf.service.messages.provisioning.InstantiateNssiRequestMessage;
import it.nextworks.nfvmano.nssmf.service.messages.provisioning.ModifyNssiRequestMessage;
import it.nextworks.nfvmano.nssmf.service.messages.provisioning.TerminateNssiRequestMessage;
import it.nextworks.nfvmano.nssmf.service.nssmanagement.NssLcmEventHandler;
import it.nextworks.nfvmano.nssmf.service.specialized.core.record.service.CoreNssInstanceRecordService;
import it.nextworks.nfvmano.nssmf.service.specialized.tn.record.elements.TransportNssInstance;
import it.nextworks.nfvmano.nssmf.service.specialized.tn.record.service.TransportNssInstanceRecordService;
import it.nextworks.nfvmano.sbi.starfish.ApiClient;
import it.nextworks.nfvmano.sbi.starfish.ApiException;
import it.nextworks.nfvmano.sbi.starfish.api.AcmApi;
import it.nextworks.nfvmano.sbi.starfish.api.NetworkSlicesApi;
import it.nextworks.nfvmano.sbi.starfish.api.PlatformApi;
import it.nextworks.nfvmano.sbi.starfish.auth.OAuthSimpleClient;
import it.nextworks.nfvmano.sbi.starfish.model.AcmLevelEnum;
import it.nextworks.nfvmano.sbi.starfish.model.NetworkSliceQualityEnum;
import it.nextworks.nfvmano.sbi.transport.odl.OdlRestClient;
import it.nextworks.nfvmano.sbi.transport.odl.interfaces.OdlProvisioningInterface;
import it.nextworks.nfvmano.sbi.transport.odl.models.*;
import org.openstack4j.api.OSClient;
import org.openstack4j.model.common.Identifier;
import org.openstack4j.model.network.Network;
import org.openstack4j.openstack.OSFactory;

import java.util.*;

/**
 *
 * Configuration parameters:
 *     - core.n9_vl (n9_net)
 *     - transport.core_ovs_node_id ()
 *     - transport.edge_ovs_node_id ()
 *     -
 */
public class SdnTransportNSSMF extends NssLcmEventHandler {


    //HANDLER Events

    @Override
    protected void processInstantiateNssRequest(InstantiateNssiRequestMessage message) throws NotExistingEntityException, FailedOperationException {
        SdnConfigPayload payload = (SdnConfigPayload) message.getInstantiateNssiRequest();
        log.info("Get Transport configuration requests - Transport slice ID: " + this.getNetworkSubSliceInstanceId());
        log.info("Target Slice ID: " + payload.getTargetNssiId().toString());
        log.info("Target Transport ID: " + payload.getTargetTransportId());
        log.info("Retrieve VLAN ID for network N9_net_" + payload.getTargetNssiId());
        OdlProvisioningInterface odlDriver = getOdlDriver();
        String n9Vl = getEnvironment().getProperty("core.n9_vl", "n9_net");
        String n9NetworkName = getNetworkNameForVl(n9Vl, payload.getTargetNssiId());
        String n9VlanId = retrieveOpenStackNetworkSegmentation(n9NetworkName);
        log.info("VLAN ID: " + n9VlanId);
        String coreOvsId = getEnvironment().getProperty("transport.core_ovs_node_id", "openflow:1");
        String edgeOvsId = getEnvironment().getProperty("transport.edge_ovs_node_id", "openflow:2");

        //CORE OVS
        String corePort = getEnvironment().getProperty("transport.core_vim_port", "3");
        //L2R_TERR_CORE
        String terrCoreFlowName = getEnvironment().getProperty("transport.core_terr_flow_name", "L2R_TERR_CORE");
        String terrCorePort = getEnvironment().getProperty("transport.core_terr_port", "10");
        FlowRequest frTerrestrialCore = generateFlowRequest(terrCoreFlowName, Integer.parseInt(terrCorePort), Integer.parseInt(corePort), null);
        odlDriver.configureTrafficFlow(coreOvsId, 0, terrCoreFlowName, frTerrestrialCore);


        //L2R_SAT_1_CORE
        String satCoreFlowName = getEnvironment().getProperty("transport.core_sat_flow_name", "L2R_SAT_SLICE_CORE");
        String sat1CoreFlowName = satCoreFlowName + "_1";
        String sat1CorePort = getEnvironment().getProperty("transport.core_sat_1_port", "2");
        FlowRequest sat1Core = generateFlowRequest(sat1CoreFlowName, Integer.parseInt(sat1CorePort), Integer.parseInt(corePort), null);
        odlDriver.configureTrafficFlow(coreOvsId, 0, sat1CoreFlowName, sat1Core);

        //L2R_SAT_2_CORE

        //String sat2CorePort = getEnvironment().getProperty("transport.core_sat_2_port", "1");
        //String sat2CoreFlowName = satCoreFlowName + "_2";
        //FlowRequest sat2Core = generateFlowRequest(sat2CoreFlowName, Integer.parseInt(sat2CorePort), Integer.parseInt(corePort), null);
        //odlDriver.configureTrafficFlow(coreOvsId, 0, sat2CoreFlowName, sat2Core);


        //EDGE OVS
        String edgePort = getEnvironment().getProperty("transport.edge_vim_port", "3");
        //FROM TERR LINK
        String terrEdgeFlowName = getEnvironment().getProperty("transport.edge_terr_flow_name", "R2L_TERR_EDGE");
        String terrEdgePort = getEnvironment().getProperty("transport.edge_terr_port", "11");
        FlowRequest frTerrestrialEdge = generateFlowRequest(terrEdgeFlowName, Integer.parseInt(terrEdgePort), Integer.parseInt(edgePort), null);
        odlDriver.configureTrafficFlow(edgeOvsId, 0, terrEdgeFlowName, frTerrestrialEdge);
        //FROM VIM


        //R2L_SAT_1_EDGE
        String satEdgeFlowName = getEnvironment().getProperty("transport.edge_sat_flow_name", "R2L_SAT_SLICE_EDGE");
        String sat1EdgeFlowName = satEdgeFlowName + "_1";
        String sat1EdgePort = getEnvironment().getProperty("transport.edge_sat_1_port", "2");
        FlowRequest sat1Edge = generateFlowRequest(sat1EdgeFlowName, Integer.parseInt(sat1EdgePort), Integer.parseInt(edgePort),null);
        odlDriver.configureTrafficFlow(coreOvsId, 0, sat1EdgeFlowName, sat1Edge);

        //L2R_SAT_2_EDGE
        //String sat2EdgeFlowName = satEdgeFlowName + "_2";
        //String sat2EdgePort = getEnvironment().getProperty("transport.edge_sat_2_port", "1");
        //FlowRequest sat2Edge = generateFlowRequest(sat2EdgeFlowName, Integer.parseInt(sat2EdgePort), Integer.parseInt(edgePort), null);
        //odlDriver.configureTrafficFlow(edgeOvsId, 0, sat2EdgeFlowName, sat2Edge);

        //List<Map<String, String>> specs = payload.getTransportSpecifications();
        String starfishSliceId = null;
        TransportConfig transportConfig =payload.getTransportConfig();
        if(transportConfig!=null && transportConfig.getTarget().equals(TransportFlowType.SATELLITE)){
            log.info("Configuring Satellite network slice");
            PlatformApi starfishClient = getStarfishClient();
            try {
                NetworkSliceQualityEnum quality = null;
                if(transportConfig.getSliceQuality()!=null){
                    quality = NetworkSliceQualityEnum.valueOf(transportConfig.getSliceQuality());
                }

                starfishSliceId = starfishClient.createNetworkSlicePlatformNetworkslicePost(
                        "SAT_"+getNetworkSubSliceInstanceId().toString(),
                        quality,
                        transportConfig.getSatelliteSubnet(),
                        transportConfig.getSatelliteGw());
                log.info("Created SATELLITE network slice:"+starfishSliceId);
            } catch (ApiException e) {
                log.error("Error during Network Slice creation on starfish:", e);
                throw new FailedOperationException(e);
            }
        }


        this.configureSdnTransport(Integer.parseInt(n9VlanId), terrEdgePort, terrCorePort);
        log.debug("Creating TN instance record");
        TransportNssInstanceRecordService service = (TransportNssInstanceRecordService) getRecordServiceFactory().getRecordService("transportNssInstanceRecordService");
        TransportNssInstance tnInstance = new TransportNssInstance(this.getNetworkSubSliceInstanceId(), starfishSliceId, Integer.parseInt(n9VlanId));
        service.createTransportNssInstanceRecord(tnInstance);
        log.info("TN slice successfully instantiated");
        NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
        notif.setSuccess(true);
        this.getEventBus().post(notif);

    }

    private void configureSdnTransport(int vlanId, String edgePort, String corePort ) throws FailedOperationException {
        log.debug("Configuring SDN transport EDGE OUTPUT port: "+edgePort+ " CORE OUTPUT port:"+corePort);
        String coreOvsId = getEnvironment().getProperty("transport.core_ovs_node_id", "openflow:1");
        String edgeOvsId = getEnvironment().getProperty("transport.edge_ovs_node_id", "openflow:2");
        String vimCorePort = getEnvironment().getProperty("transport.core_vim_port", "3");
        String vimEdgePort = getEnvironment().getProperty("transport.edge_vim_port", "3");


        OdlProvisioningInterface odlDriver = getOdlDriver();
        String vimEdgeFlowName = getEnvironment().getProperty("transport.edge_vim_flow_name", "L2R_EDGE");
        FlowRequest frVimEdge = generateFlowRequest(vimEdgeFlowName,
                Integer.parseInt(vimEdgePort),
                Integer.parseInt(edgePort),
                Integer.toString(vlanId));
        odlDriver.configureTrafficFlow(edgeOvsId, 0, vimEdgeFlowName, frVimEdge);

        String vimCoreFlowName = getEnvironment().getProperty("transport.core_vim_flow_name", "R2L_CORE");

        FlowRequest frVimCore = generateFlowRequest(vimCoreFlowName,
                Integer.parseInt(vimCorePort),
                Integer.parseInt(corePort),
                Integer.toString(vlanId));
        odlDriver.configureTrafficFlow(coreOvsId, 0, vimCoreFlowName, frVimCore);
    }


    @Override
    protected void processTerminateNssRequest(TerminateNssiRequestMessage message) throws NotExistingEntityException, FailedOperationException {
        log.info("Received request to terminate TN NSS");
        OdlProvisioningInterface odlClient = getOdlDriver();
        String coreOvsId = getEnvironment().getProperty("transport.core_ovs_node_id", "openflow:1");
        String edgeOvsId = getEnvironment().getProperty("transport.edge_ovs_node_id", "openflow:2");
        String terrCoreFlowName = getEnvironment().getProperty("transport.core_terr_flow_name", "L2R_TERR_CORE");
        String vimCoreFlowName = getEnvironment().getProperty("transport.core_vim_flow_name", "L2R_CORE");
        String vimEdgeFlowName = getEnvironment().getProperty("transport.edge_vim_flow_name", "R2L_EDGE");
        String satCoreFlowName = getEnvironment().getProperty("transport.core_sat_flow_name", "L2R_SAT_SLICE_CORE");
        String sat1CoreFlowName = satCoreFlowName+"_1";
        //String sat2CoreFlowName = satCoreFlowName+"_2";
        String terrEdgeFlowName = getEnvironment().getProperty("transport.edge_terr_flow_name", "R2L_TERR_EDGE");
        String satEdgeFlowName = getEnvironment().getProperty("transport.edge_sat_flow_name", "R2L_SAT_SLICE_EDGE");
        String sat1EdgeFlowName = satEdgeFlowName+"_1";
        //String sat2EdgeFlowName = satEdgeFlowName+"_2";

        odlClient.deleteTrafficFlow(coreOvsId, 0, terrCoreFlowName);
        odlClient.deleteTrafficFlow(coreOvsId, 0, sat1CoreFlowName);
        //odlClient.deleteTrafficFlow(coreOvsId, 0, sat2CoreFlowName);
        odlClient.deleteTrafficFlow(coreOvsId, 0, vimCoreFlowName);

        odlClient.deleteTrafficFlow(edgeOvsId, 0, terrEdgeFlowName);
        odlClient.deleteTrafficFlow(edgeOvsId, 0, sat1EdgeFlowName);
        //odlClient.deleteTrafficFlow(edgeOvsId, 0, sat2EdgeFlowName);
        odlClient.deleteTrafficFlow(edgeOvsId, 0, vimEdgeFlowName);

        NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
        notif.setSuccess(true);
        this.getEventBus().post(notif);
    }

    @Override
    protected void processModifyNssRequest(ModifyNssiRequestMessage message) throws NotExistingEntityException, FailedOperationException {

        SdnConfigPayload payload = (SdnConfigPayload)message.getModifyNssiRequest();
        //Step1 Get Vlan ID by Subslice ID from OpenStack
        TransportNssInstanceRecordService service = (TransportNssInstanceRecordService) getRecordServiceFactory().getRecordService("transportNssInstanceRecordService");
        TransportNssInstance tnInstance = service.getTransportNssInstance(this.getNetworkSubSliceInstanceId());
        log.info("Get Transport configuration requests - Transport slice ID: " + this.getNetworkSubSliceInstanceId());
        //log.info("Target Slice ID: "+ payload.getTargetNssiId());
        log.info("Requested Action: " + payload.getConfigurationActionType());

        if(payload.getConfigurationActionType() == ConfigurationActionType.SLICE_TRANSFER) {
            log.info("Processing SLICE_TRANSFER request");
            if (payload.getTransportConfig() != null)
                log.info("" + payload.getTransportConfig());

            log.info("Retrieve existing config from SDN Controller for slice " + this.getNetworkSubSliceInstanceId());
            Map<String, String> flowTransfers = payload.getSliceFlowTransfers();
            for (String origin: flowTransfers.keySet()){
                String target = flowTransfers.get(origin);
                log.info("SLICE TRANSFER from: "+origin+ " to:"+target);
                if(origin.equals("TERRESTRIAL")&&target.equals("SATELLITE")){

                    String sat1EdgePort = getEnvironment().getProperty("transport.edge_sat_1_port", "2");
                    String sat1CorePort = getEnvironment().getProperty("transport.core_sat_1_port", "2");
                    configureSdnTransport(tnInstance.getN9VlanId(),sat1EdgePort, sat1CorePort );
                    NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
                    notif.setSuccess(true);
                    this.getEventBus().post(notif);
                }else if(target.equals("TERRESTRIAL")&&origin.equals("SATELLITE")){
                    String terrEdgePort = getEnvironment().getProperty("transport.edge_terr_port", "11");
                    String terrCorePort = getEnvironment().getProperty("transport.core_terr_port", "10");
                    configureSdnTransport(tnInstance.getN9VlanId(),terrEdgePort, terrCorePort );
                    NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
                    notif.setSuccess(true);
                    this.getEventBus().post(notif);
                }else{
                    log.warn("UNKNOWN SLICE TRANSFER TARGET/ORIGIN");
                    NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
                    notif.setSuccess(true);
                    this.getEventBus().post(notif);
                }
            }
        }else if(payload.getConfigurationActionType()==ConfigurationActionType.SATELLITE_NETWORK_CONFIGURATION) {
            log.info("Received request to re-configure satellite network slice");

            try {
                if(payload.getTransportConfig().getSatelliteGw()!=null && !payload.getTransportConfig().getSatelliteGw().isEmpty()){
                    NetworkSlicesApi client = getStarfishNSClient();
                    client.migrateNetworkSliceNetworksliceMigratePut(tnInstance.getSatelliteNetworkId(), payload.getTransportConfig().getSatelliteGw());
                }
                Map<String, String> acmConfig= payload.getTransportConfig().getAcmConfig();
                if(acmConfig!=null && !acmConfig.isEmpty()){
                    AcmApi client = getStarfishAcmClient();
                    client.setAcmLevelPlatformAcmSimplePut(AcmLevelEnum.valueOf(acmConfig.get("acm-level")));

                }

                NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
                notif.setSuccess(true);
                this.getEventBus().post(notif);
            } catch (ApiException e) {
                log.error("Error during Starfish network slice reconfiguration:",e);
                NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
                notif.setSuccess(false);
                this.getEventBus().post(notif);
            }
        }else  {
            log.warn("NEtwork Slice reconfiguration action NOT supported. IGNORING");
            NssiStatusChangeNotiticationMessage notif = new NssiStatusChangeNotiticationMessage();
            notif.setSuccess(true);
            this.getEventBus().post(notif);
        }

    }


    private FlowRequest generateFlowRequest(String flowName, int inPort,  int outputConnector, String vlanId){
        log.debug("Generating ODL Flow Request: "+flowName);
        FlowRequest fr = new FlowRequest();
        FlowNodeInventoryFlow flow = new FlowNodeInventoryFlow();
        flow.setId(flowName);
        flow.setPriority(2);
        flow.setTable_id(0);
        flow.setCookie(1);
        flow.setFlowName(flowName);
        Match match = new Match();
        match.setInPort(Integer.toString(inPort));
        if(vlanId!=null){
            match.setVlanMatch(new VlanMatch(new VlanId(true, vlanId)));
        }
        flow.setMatch(match);
        Instructions instructions = new Instructions();
        ArrayList<Instruction> instructionsLst = new ArrayList<>();
        Instruction instruction  = new Instruction();
        instruction.setOrder(0);
        ApplyActions applyAction =  new ApplyActions();
        ArrayList<Action> actions= new ArrayList<>();
        Action action = new Action();
        action.setOrder(0);
        OutputAction outputAction= new OutputAction();
        outputAction.setOutputNodeConnector(Integer.toString(outputConnector));
        action.setOutputAction(outputAction);
        actions.add(action);
        applyAction.setAction(actions);
        instruction.setApplyActions(applyAction);
        instructionsLst.add(instruction);
        instructions.setInstruction(instructionsLst);
        flow.setInstructions(instructions);
        ArrayList<FlowNodeInventoryFlow> flows = new ArrayList<>();
        flows.add(flow);
        fr.setFlowNodeInventoryFlow(flows);
        return fr;
    }

    private String retrieveOpenStackNetworkSegmentation(String networkName) throws FailedOperationException {
        log.debug("Retrieving Openstack Network Segmentation for: "+ networkName);
        boolean dummy = Boolean.parseBoolean(getEnvironment().getProperty("vim.dummy", "false"));
        if(!dummy){
            OSClient.OSClientV3 os = getOpenStackClient();
            log.debug("Successfully authenticated with VIM");
            List<? extends Network> networks = os.networking().network().list();
            Optional<? extends Network> targetNetwork = networks.stream().filter(n -> n.getName().equals(networkName))
                    .findFirst();
            if(targetNetwork.isPresent()){
                log.debug("Retrieved network information for: "+networkName+ " - "+targetNetwork.get().getId());
                return targetNetwork.get().getProviderSegID();
            }else throw new FailedOperationException("Could not find VIM network with name:"+networkName);

        }else return getEnvironment().getProperty("core.n9_segmentation", "20");

    }

    private  String getNetworkNameForVl(String vl, UUID targetNssiId){
        return vl+"_"+targetNssiId.toString();
    }

    private OdlProvisioningInterface getOdlDriver(){
        log.debug("Creating ODL Client");
        String address  = getEnvironment().getProperty("odl.address");
        String username  = getEnvironment().getProperty("odl.username");
        String password = getEnvironment().getProperty("odl.password");

        return new OdlRestClient(address, username, password);
    }

    private OSClient.OSClientV3 getOpenStackClient() throws FailedOperationException {
        log.debug("Creating OpenStack Client");
        try{
            String address  = getEnvironment().getProperty("vim.address");
            String username  = getEnvironment().getProperty("vim.username");
            String password = getEnvironment().getProperty("vim.password");
            String project = getEnvironment().getProperty("vim.project");
            String domain = getEnvironment().getProperty("vim.domain", "default");
            boolean debugVim = Boolean.parseBoolean(getEnvironment().getProperty("vim.debug", "false"));
            OSFactory.enableHttpLoggingFilter(debugVim);
            Identifier domainIdentifier = Identifier.byName(domain);
            Identifier projectIdentifier = Identifier.byName(project);
            //boolean dummy = Boolean.parseBoolean(getEnvironment().getProperty("vim.dummy", "false"));
            OSClient.OSClientV3 os = OSFactory.builderV3()
                    .endpoint(address)
                    .credentials(username, password, domainIdentifier)
                    .scopeToProject(projectIdentifier, domainIdentifier)

                    .authenticate();
            log.debug("Successfully authenticated OpenStack client");
            return os;
        }catch(Exception e){
            log.error("Error while interacting with OpenStack:",e);
            throw new FailedOperationException(e.getMessage());

        }

    }

    private AcmApi getStarfishAcmClient() throws FailedOperationException {
        log.debug("Creating starfish client");
        AcmApi client  = new AcmApi();
        String username  = getEnvironment().getProperty("starfish.username");
        String password  = getEnvironment().getProperty("starfish.password");
        String baseUrl  = getEnvironment().getProperty("starfish.baseurl");
        OAuthSimpleClient oAuthSimpleClient = new OAuthSimpleClient(baseUrl+"/token", username, password,null);
        ApiClient apiClient = new ApiClient();
        apiClient.setUsername(username);
        apiClient.setPassword(password);
        apiClient.setDebugging(true);
        String token = oAuthSimpleClient.getToken();
        apiClient.setAccessToken(token);
        apiClient.setBasePath(getEnvironment().getProperty("starfish.baseurl"));
        client.setApiClient(apiClient);
        return client;
    }

    private NetworkSlicesApi getStarfishNSClient() throws FailedOperationException {
        log.debug("Creating starfish client");
        NetworkSlicesApi client  = new NetworkSlicesApi();
        String username  = getEnvironment().getProperty("starfish.username");
        String password  = getEnvironment().getProperty("starfish.password");
        String baseUrl  = getEnvironment().getProperty("starfish.baseurl");
        OAuthSimpleClient oAuthSimpleClient = new OAuthSimpleClient(baseUrl+"/token", username, password,null);
        ApiClient apiClient = new ApiClient();
        apiClient.setUsername(username);
        apiClient.setPassword(password);
        apiClient.setDebugging(true);
        String token = oAuthSimpleClient.getToken();
        apiClient.setAccessToken(token);
        apiClient.setBasePath(baseUrl);
        client.setApiClient(apiClient);
        return client;
    }
    private PlatformApi getStarfishClient() throws FailedOperationException {
        log.debug("Creating starfish client");
        PlatformApi client  = new PlatformApi();
        String username  = getEnvironment().getProperty("starfish.username");
        String password  = getEnvironment().getProperty("starfish.password");
        String baseUrl  = getEnvironment().getProperty("starfish.baseurl");

        ApiClient apiClient = new ApiClient();
        apiClient.setUsername(username);
        apiClient.setPassword(password);
        apiClient.setDebugging(true);
        OAuthSimpleClient oAuthSimpleClient = new OAuthSimpleClient(baseUrl+"/token", username, password,null);
        String token = oAuthSimpleClient.getToken();
        apiClient.setAccessToken(token);
        apiClient.setBasePath(baseUrl);
        client.setApiClient(apiClient);
        return client;
    }

}

package it.nextworks.nfvmano.nssmf.service.specialized.core.smf.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NetworkSliceTypeObject {

    @JsonProperty("network-slice-type")
    public NetworkSliceType networkSliceType;

    public NetworkSliceTypeObject() {
    }

    public NetworkSliceTypeObject(String type) {
        this.networkSliceType= NetworkSliceType.valueOf(type);
    }

    public NetworkSliceTypeObject(NetworkSliceType networkSliceType) {
        this.networkSliceType = networkSliceType;
    }


    public NetworkSliceType getNetworkSliceType() {
        return networkSliceType;
    }
}

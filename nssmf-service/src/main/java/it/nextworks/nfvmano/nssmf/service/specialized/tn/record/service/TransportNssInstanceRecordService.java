package it.nextworks.nfvmano.nssmf.service.specialized.tn.record.service;


import it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.nssmf.service.specialized.tn.record.elements.TransportNssInstance;
import it.nextworks.nfvmano.nssmf.service.specialized.tn.record.repo.TransportNssInstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class TransportNssInstanceRecordService {

    @Autowired
    private TransportNssInstanceRepository transportNssInstanceRepository;

    public void createTransportNssInstanceRecord(TransportNssInstance instance){
        transportNssInstanceRepository.saveAndFlush(instance);
    }

    public TransportNssInstance getTransportNssInstance(UUID nssiId)  throws NotExistingEntityException {
        Optional<TransportNssInstance> tnInstance = transportNssInstanceRepository.findByNssiId(nssiId);
        if(tnInstance.isPresent())
            return  tnInstance.get();
        else throw new NotExistingEntityException("Transport NSS instance not found for NSSI:"+nssiId);
    }

}

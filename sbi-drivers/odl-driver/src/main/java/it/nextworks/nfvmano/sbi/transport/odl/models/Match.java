package it.nextworks.nfvmano.sbi.transport.odl.models; 
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
public class Match{
    @JsonProperty("in-port") 
    public String inPort;

    @JsonProperty("vlan-match")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private VlanMatch vlanMatch;


    public VlanMatch getVlanMatch() {
        return vlanMatch;
    }

    public void setVlanMatch(VlanMatch vlanMatch) {
        this.vlanMatch = vlanMatch;
    }

    public String getInPort() {
        return inPort;
    }

    public void setInPort(String inPort) {
        this.inPort = inPort;
    }
}

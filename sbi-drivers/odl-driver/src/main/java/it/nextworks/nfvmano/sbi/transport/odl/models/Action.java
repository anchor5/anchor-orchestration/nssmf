package it.nextworks.nfvmano.sbi.transport.odl.models; 
import com.fasterxml.jackson.annotation.JsonProperty; 
public class Action{
    public int order;
    @JsonProperty("output-action") 
    public OutputAction outputAction;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public OutputAction getOutputAction() {
        return outputAction;
    }

    public void setOutputAction(OutputAction outputAction) {
        this.outputAction = outputAction;
    }
}

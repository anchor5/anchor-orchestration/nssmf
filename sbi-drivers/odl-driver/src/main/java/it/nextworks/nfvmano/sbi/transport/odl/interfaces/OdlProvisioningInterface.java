package it.nextworks.nfvmano.sbi.transport.odl.interfaces;

import it.nextworks.nfvmano.libs.vs.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.sbi.transport.odl.models.FlowRequest;

public interface OdlProvisioningInterface {

    void configureTrafficFlow(String nodeId, int tableNumber, String flowName, FlowRequest request) throws FailedOperationException;

    void deleteTrafficFlow(String nodeId, int tableNumber, String flowName) throws FailedOperationException;
}

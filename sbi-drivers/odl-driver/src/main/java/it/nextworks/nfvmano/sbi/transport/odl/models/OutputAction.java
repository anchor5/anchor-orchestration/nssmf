package it.nextworks.nfvmano.sbi.transport.odl.models; 
import com.fasterxml.jackson.annotation.JsonProperty; 
public class OutputAction{
    @JsonProperty("output-node-connector") 
    public String outputNodeConnector;

    public String getOutputNodeConnector() {
        return outputNodeConnector;
    }

    public void setOutputNodeConnector(String outputNodeConnector) {
        this.outputNodeConnector = outputNodeConnector;
    }
}

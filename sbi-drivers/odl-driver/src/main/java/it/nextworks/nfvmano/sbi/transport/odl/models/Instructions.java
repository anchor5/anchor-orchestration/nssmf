package it.nextworks.nfvmano.sbi.transport.odl.models; 
import java.util.ArrayList;
public class Instructions{
    public ArrayList<Instruction> instruction;

    public ArrayList<Instruction> getInstruction() {
        return instruction;
    }

    public void setInstruction(ArrayList<Instruction> instruction) {
        this.instruction = instruction;
    }
}

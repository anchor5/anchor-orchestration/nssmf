package it.nextworks.nfvmano.sbi.transport.odl.models;

import java.util.ArrayList;

public class ApplyActions{
    public ArrayList<Action> action;

    public ArrayList<Action> getAction() {
        return action;
    }

    public void setAction(ArrayList<Action> action) {
        this.action = action;
    }
}

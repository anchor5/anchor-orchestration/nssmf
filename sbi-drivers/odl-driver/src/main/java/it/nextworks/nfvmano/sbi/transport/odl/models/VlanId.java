package it.nextworks.nfvmano.sbi.transport.odl.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VlanId {
    @JsonProperty("vlan-id-present")
    private boolean vlanIdPresent;

    @JsonProperty("vlan-id")
    private String vlanId;

    public VlanId() {
    }

    public VlanId(boolean vlanIdPresent, String vlanId) {
        this.vlanIdPresent = vlanIdPresent;
        this.vlanId = vlanId;
    }

    public boolean isVlanIdPresent() {
        return vlanIdPresent;
    }

    public String getVlanId() {
        return vlanId;
    }
}

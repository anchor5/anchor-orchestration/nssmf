package it.nextworks.nfvmano.sbi.transport.odl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.nfvmano.libs.vs.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.sbi.transport.odl.interfaces.OdlProvisioningInterface;
import it.nextworks.nfvmano.sbi.transport.odl.models.FlowRequest;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

public class OdlRestClient implements OdlProvisioningInterface {

    protected static final Logger log = LoggerFactory.getLogger(OdlRestClient.class);
    private String odlBaseUrl;
    private String username;
    private String password;

    public OdlRestClient(String odlBaseUrl, String username, String password){
        this.odlBaseUrl= odlBaseUrl;
        this.username = username;
        this.password = password;
    }

    @Override
    public void configureTrafficFlow(String nodeId, int tableNumber, String flowName, FlowRequest request) throws FailedOperationException {
        log.info("Received request to configure traffic flow");
        RestTemplate restTemplate  = new RestTemplate();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String url = odlBaseUrl+"/config/opendaylight-inventory:nodes/node/"+nodeId+"/table/"+tableNumber+"/flow/"+flowName;
            String configRequestStr = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(request);
            log.debug(configRequestStr);
            HttpHeaders httpHeaders = new HttpHeaders(){{
                String auth = username + ":" + password;
                byte[] encodedAuth = Base64.encodeBase64(
                        auth.getBytes(Charset.forName("US-ASCII")) );
                String authHeader = "Basic " + new String( encodedAuth );
                setContentType(MediaType.APPLICATION_JSON);

                set( "Authorization", authHeader );
            }};
            HttpEntity<String> httpEntity = new HttpEntity<String>(configRequestStr,httpHeaders );
            restTemplate.put(url, httpEntity, String.class );
        } catch (JsonProcessingException e) {
            throw new FailedOperationException(e);
        }
    }

    @Override
    public void deleteTrafficFlow(String nodeId, int tableNumber, String flowName) throws FailedOperationException {
        log.info("Received request to delete traffic flow");
        String url = odlBaseUrl+"/config/opendaylight-inventory:nodes/node/"+nodeId+"/table/"+tableNumber+"/flow/"+flowName;
        HttpHeaders httpHeaders = new HttpHeaders(){{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            setContentType(MediaType.APPLICATION_JSON);

            set( "Authorization", authHeader );
        }};
        RestTemplate restTemplate  = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity<String>(httpHeaders );
        restTemplate.delete(url, httpEntity, String.class );
    }
}

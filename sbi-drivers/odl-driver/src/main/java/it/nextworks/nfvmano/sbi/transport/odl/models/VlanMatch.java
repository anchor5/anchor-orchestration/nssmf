package it.nextworks.nfvmano.sbi.transport.odl.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VlanMatch {

    @JsonProperty("vlan-id")
    private VlanId vlanId;

    public VlanMatch() {
    }

    public VlanMatch(VlanId vlanId) {
        this.vlanId = vlanId;
    }

    public VlanId getVlanId() {
        return vlanId;
    }
}

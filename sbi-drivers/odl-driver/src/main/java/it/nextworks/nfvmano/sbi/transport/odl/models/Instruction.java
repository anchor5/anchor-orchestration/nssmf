package it.nextworks.nfvmano.sbi.transport.odl.models; 
import com.fasterxml.jackson.annotation.JsonProperty; 
public class Instruction{
    public int order;
    @JsonProperty("apply-actions") 
    public ApplyActions applyActions;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public ApplyActions getApplyActions() {
        return applyActions;
    }

    public void setApplyActions(ApplyActions applyActions) {
        this.applyActions = applyActions;
    }
}

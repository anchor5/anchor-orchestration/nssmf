package it.nextworks.nfvmano.sbi.transport.odl.models; 
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
public class FlowRequest {
    @JsonProperty("flow-node-inventory:flow") 
    public ArrayList<FlowNodeInventoryFlow> flowNodeInventoryFlow;

    public ArrayList<FlowNodeInventoryFlow> getFlowNodeInventoryFlow() {
        return flowNodeInventoryFlow;
    }

    public void setFlowNodeInventoryFlow(ArrayList<FlowNodeInventoryFlow> flowNodeInventoryFlow) {
        this.flowNodeInventoryFlow = flowNodeInventoryFlow;
    }
}

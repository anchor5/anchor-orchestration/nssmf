/*
 * Hypercube API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package it.nextworks.nfvmano.sbi.starfish.model;

import java.io.IOException;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * An enumeration.
 */
@JsonAdapter(GatewayType.Adapter.class)
public enum GatewayType {
  FORWARD("forward"),
  RETURN("return"),
  CO_HOSTED("co-hosted");

  private String value;

  GatewayType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  public static GatewayType fromValue(String input) {
    for (GatewayType b : GatewayType.values()) {
      if (b.value.equals(input)) {
        return b;
      }
    }
    return null;
  }

  public static class Adapter extends TypeAdapter<GatewayType> {
    @Override
    public void write(final JsonWriter jsonWriter, final GatewayType enumeration) throws IOException {
      jsonWriter.value(String.valueOf(enumeration.getValue()));
    }

    @Override
    public GatewayType read(final JsonReader jsonReader) throws IOException {
      Object value = jsonReader.nextString();
      return GatewayType.fromValue((String)(value));
    }
  }
}
